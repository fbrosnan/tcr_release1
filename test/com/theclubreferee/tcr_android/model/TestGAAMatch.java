package com.theclubreferee.tcr_android.model;

import java.util.UUID;

import junit.framework.TestCase;

import org.junit.Test;



public class TestGAAMatch extends TestCase{
	@Test
	public void testGAAMatch()
	{
		GaaMatch gaaMatch = new GaaMatch();
		
		UUID guid1=UUID.randomUUID();
		UUID guid2=UUID.randomUUID();
		Team team1 = new Team("Liverpool",  "Crap", 1, guid1);
		Team team2 = new Team("Man Utd", "Brilliant", 2, guid2);
		
		gaaMatch.setHomeTeam(team1);
		gaaMatch.setAwayTeam(team2);
		
		assertTrue(team1.getTeamName().equals("Liverpool"));
		assertTrue(team2.getTeamName().equals("Man Utd"));
		assertTrue(team1.getTeamDescription().equals("Crap"));
		assertTrue(team2.getTeamDescription().equals("Brilliant"));
		assertTrue(team1.getTeamID() == 1);
		assertTrue(team2.getTeamID() == 2);
		assertTrue(team1.getTeamGuid()==guid1);
		assertTrue(team2.getTeamGuid()==guid2);
		
		check(team1,0,0,0,0,0,team2,0,0,0,0,0);
		
		((GAAPlayer)(team1.getPlayerByNumber(1))).incrementBlackCardCount();
		check(team1,1,0,0,0,0,team2,0,0,0,0,0);
		
		
		((GAAPlayer)(team2.getPlayerByNumber(5))).incrementBlackCardCount();
		check(team1,1,0,0,0,0,team2,1,0,0,0,0);
		
		((GAAPlayer)(team1.getPlayerByNumber(2))).incrementYellowCardCount();
		check(team1,1,1,0,0,0,team2,1,0,0,0,0);
		
		((GAAPlayer)(team2.getPlayerByNumber(6))).incrementYellowCardCount();
		check(team1,1,1,0,0,0,team2,1,1,0,0,0);
		
		((GAAPlayer)(team1.getPlayerByNumber(3))).incrementRedCardCount();
		check(team1,1,1,1,0,0,team2,1,1,0,0,0);
		
		((GAAPlayer)(team2.getPlayerByNumber(7))).incrementRedCardCount();
		check(team1,1,1,1,0,0,team2,1,1,1,0,0);
		
		((GAAPlayer)(team1.getPlayerByNumber(4))).incrementGoalCount();
		check(team1,1,1,1,1,0,team2,1,1,1,0,0);
		
		((GAAPlayer)(team2.getPlayerByNumber(8))).incrementGoalCount();
		check(team1,1,1,1,1,0,team2,1,1,1,1,0);
		
		((GAAPlayer)(team1.getPlayerByNumber(5))).incrementPointCount();
		check(team1,1,1,1,1,1,team2,1,1,1,1,0);
		
		((GAAPlayer)(team2.getPlayerByNumber(9))).incrementPointCount();
		check(team1,1,1,1,1,1,team2,1,1,1,1,1);
		
		((GAAPlayer)(team1.getPlayerByNumber(6))).incrementGoalCount();
		check(team1,1,1,1,2,1,team2,1,1,1,1,1);
		
		((GAAPlayer)(team2.getPlayerByNumber(10))).incrementGoalCount();
		check(team1,1,1,1,2,1,team2,1,1,1,2,1);
		
		((GAAPlayer)(team1.getPlayerByNumber(7))).incrementPointCount();
		check(team1,1,1,1,2,2,team2,1,1,1,2,1);
		
		((GAAPlayer)(team2.getPlayerByNumber(11))).incrementPointCount();
		check(team1,1,1,1,2,2,team2,1,1,1,2,2);
		
		
}
	
	private void check(Team team1, int i, int j, int k, int l, int m,
			Team team2, int n, int o, int p, int q, int r) {
		assertTrue(team1.getTotalBlackCards()==i);
		assertTrue(team1.getTotalYellowCards()==j);
		assertTrue(team1.getTotalRedCards()==k);
		assertTrue(team1.getTotalGoals()==l);
		assertTrue(team1.getTotalPoints()==m);
		
		assertTrue(team2.getTotalBlackCards()==n);
		assertTrue(team2.getTotalYellowCards()==o);
		assertTrue(team2.getTotalRedCards()==p);
		assertTrue(team2.getTotalGoals()==q);
		assertTrue(team2.getTotalPoints()==r);
		
	}
}
