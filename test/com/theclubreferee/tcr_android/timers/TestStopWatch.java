package com.theclubreferee.tcr_android.timers;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestStopWatch {

	
	public class TestWatchActions extends WatchActions {

		public int startCount;
		public int stopCount;
		public int tickCount;
		public int pauseCount;
		
		@Override
		public void performTickAction() {
			tickCount++;
			
		}

		@Override
		public void performStopAction() {
			stopCount++;
			
		}

		@Override
		public void performPauseAction() {
			pauseCount++;
		}

		@Override
		public void performStartAction() {
			startCount++;
		}
		
	}
	
	
	
	
	
	@Test
	public void testStartStopWatch() {
		
		TestWatchActions testActions = new TestWatchActions();
		StopWatch myTestWatch3 = new StopWatch(60,"StopWatch3",testActions,false);
		myTestWatch3.startWatch();
		
		
		try {
			Thread.sleep(65000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertEquals(1,testActions.startCount);
		assertEquals(1,testActions.stopCount);
		assertEquals(60,testActions.tickCount);
		
		
		TestWatchActions testActions2 = new TestWatchActions();
		StopWatch myTestWatch4 = new StopWatch(20,"StopWatch3",testActions2,true);
		myTestWatch4.startWatch();
		
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertEquals(1,testActions2.startCount);
		assertTrue(testActions2.tickCount > 20);
		assertEquals(0,testActions2.stopCount);
		
		myTestWatch4.stopWatch();
		
		assertEquals(1,testActions2.stopCount);
		
		
		
		
	}

	@Test
	public void testPauseWatch() {
		TestWatchActions testActions = new TestWatchActions();
		StopWatch myTestWatch3 = new StopWatch(65,"StopWatch3",testActions,false);
		myTestWatch3.startWatch();
		
		try {
			Thread.sleep(25000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		myTestWatch3.pauseWatch();
		
		int myTickCount = testActions.tickCount;
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int mySecondTickCount = testActions.tickCount;
		System.out.println("myTickCount1="+mySecondTickCount);
		
		assertEquals(myTickCount,mySecondTickCount);
		
		myTestWatch3.startWatch();
		
		try {
			Thread.sleep(25000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		myTestWatch3.pauseWatch();
		System.out.println("myTickCount2="+testActions.tickCount);
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		myTestWatch3.startWatch();
		
		try {
			Thread.sleep(25000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		assertEquals(1,testActions.stopCount);
		System.out.println("myTickCount3="+testActions.tickCount);
	}

	@Test
	public void testConstructStopWatch() {
		
		StopWatch myTestWatch1 = new StopWatch(60,"StopWatch1",null,false);
		StopWatch myTestWatch2 = new StopWatch(120,"StopWatch2",null,true);
		StopWatch myTestWatch3 = new StopWatch(180,"StopWatch3",new TestWatchActions(),false);
		StopWatch myTestWatch4 = new StopWatch(30,"StopWatch4",new TestWatchActions(),true);
		
		assertNotNull(myTestWatch1);
		assertNotNull(myTestWatch2);
		assertNotNull(myTestWatch3);
		assertNotNull(myTestWatch4);
		
		assertEquals(myTestWatch1.getId(),"StopWatch1");
		assertEquals(myTestWatch2.getId(),"StopWatch2");
		assertEquals(myTestWatch3.getId(),"StopWatch3");
		assertEquals(myTestWatch4.getId(),"StopWatch4");
		
		assertEquals(myTestWatch1.getElapsedTime(),0);
		assertEquals(myTestWatch2.getElapsedTime(),0);
		assertEquals(myTestWatch3.getElapsedTime(),0);
		assertEquals(myTestWatch4.getElapsedTime(),0);
		
		assertEquals(myTestWatch1.isIndefinite(),false);
		assertEquals(myTestWatch2.isIndefinite(),true);
		assertEquals(myTestWatch3.isIndefinite(),false);
		assertEquals(myTestWatch4.isIndefinite(),true);
		
		
		
		
		
	}


}
