/**
 * 
 */
package com.theclubreferee.tcr_android;

import java.util.ArrayList;
import java.util.List;

/**
 * A match report representing the underlying data 
 * for a match report.
 * A collection of match report records.
 * @author francisbrosnan
 *
 */
public class MatchReport {

	private List<MatchEvent> records = new ArrayList<MatchEvent>();
	
	public List<MatchEvent> getRecords(){
		return records;
	}
	
	@Override
	public String toString(){
		StringBuilder retBuilder = new StringBuilder();
		for (MatchEvent rec : records){
			retBuilder.append(rec.toString() + "\n");
		}
		return retBuilder.toString();
	}
	
	
}
