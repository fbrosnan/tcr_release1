package com.theclubreferee.tcr_android;

public class MatchEventType {
	private int _evTypeId;
	public int get_evTypeId() {
		return _evTypeId;
	}
	public void set_evTypeId(int _evTypeId) {
		this._evTypeId = _evTypeId;
	}
	
	private String _evName;
	public String get_evName() {
		return _evName;
	}
	public void set_evName(String _evName) {
		this._evName = _evName;
	}
	
	private int _evPoints = 0;
	public int get_evPoints() {
		return _evPoints;
	}
	public void set_evPoints(int _evPoints) {
		this._evTypeId = _evPoints;
	}
	
	private int _evGoals = 0;
	public int get_evGoals() {
		return _evGoals;
	}
	public void set_evGoals(int _evGoals) {
		this._evGoals = _evGoals;
	}
	
	private int _evCardId;
	public int get_evCardId() {
		return _evCardId;
	}
	public void set_evCardId(int _evCardId) {
		this._evCardId = _evCardId;
	}
	
}
