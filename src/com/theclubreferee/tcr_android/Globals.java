package com.theclubreferee.tcr_android;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

import com.theclubreferee.tcr_android.timers.TimersRecord;

public class Globals {

	//Ray
	//responses from WCF
	public static JSONObject jo;
	public static JSONObject AppVersion;
	public static String TeamNameResponse;
	public static String UserNewNameResponse;
	public static String UserUpdateNameResponse;
	public static String MatchEventResponse;
	public static String NewMatchResponse;
	
	//Directory to store serialized data files.
	public static File BaseDir; 
	
	//Offline/Online ?
	public static boolean isServerUp;
	
	
	//Ray.
	//Match Events
	public static MatchEvent CurrentEvent = null;	//working with an event now to be saved to collection (scores, carding etc)
	public static MatchEvents AllMatchEvents;  			//any event that happens in a game will be added to this collection.
	
	public static MatchReport MatchReport = new MatchReport();
	
	public static TimersRecord TimersRecord = new TimersRecord();
	
	
	//format a date for ReST over WCF
	public static String FormatDateWCF(Date d){
		
		
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd'T'hh:mm:ss");
			
		return ft.format(d);
		
	}
	
	public static boolean isNumeric(String str)
	{
	    for (char c : str.toCharArray())
	    {
	        if (!Character.isDigit(c)) return false;
	    }
	    return true;
	}
	
	/**
	 * convert a time in elapsed seconds into a time stamp 
	 * of the form MM:SS.
	 */
	public static String convertElapsedTimeToMatchTime(long elapsedTime){
		final int elapsedMins = (int) (elapsedTime / 60);
		final int elapsedSecs = (int) (elapsedTime % 60);
		return(String.format("%02d:%02d", elapsedMins,elapsedSecs));
	}
	
	public static String convertElapsedTimeToMatchTimeExtraTime(long matchTime, long extraTime){
		final int elapsedMins = (int) (matchTime / 60);
		final int elapsedSecs = (int) (matchTime % 60);
		
		final int elapsedMinsExtra = (int) (extraTime / 60);
		final int elapsedSecsExtra = (int) (extraTime % 60);
		
		return(String.format("%02d:%02d+%02d:%02d", elapsedMins,elapsedSecs,elapsedMinsExtra,elapsedSecsExtra));
		
	}
	
	
}
