//This class will makes all the calls to the Web Services (WCF).

package com.theclubreferee.tcr_android;

import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;



//This class is for communication with the Web Services
//for sending data to the web

public class Tweets implements Runnable {

	
	public JSONObject emp = null;
	


	@Override
	public void run() {
		try {

			// http get request
			// EMPLOYEE_SERVICE_URI +=
			// "matchGuid=0001e745-22f4-4de7-993d-5700d1c99252";
			// EMPLOYEE_SERVICE_URI += "&homeTeamName=HomeTeam";
			// EMPLOYEE_SERVICE_URI += "&awayTeamName=AwayTeam";
			// EMPLOYEE_SERVICE_URI += "&venueName=Jellybean";
			// EMPLOYEE_SERVICE_URI += "&matchTitle=AndroidTest";
			// EMPLOYEE_SERVICE_URI += "&kickOffTime=01012001140000PM";
			// EMPLOYEE_SERVICE_URI += "&refName=Ted";
			// EMPLOYEE_SERVICE_URI += "&sportId=1";
			// EMPLOYEE_SERVICE_URI +=
			// "&appGuid=0001e745-22f4-4de7-993d-5700d1c11111";
			// EMPLOYEE_SERVICE_URI += "&matchKey=abcd";

			DefaultHttpClient client = new DefaultHttpClient();
			String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/GetEmployee/?key=22";
			HttpGet request = new HttpGet(EMPLOYEE_SERVICE_URI);

			// set the header to get the data in JSON format
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");

			// get the response
			HttpResponse response = client.execute(request);

			HttpEntity entity = response.getEntity();

			// if entity content lenght 0, means no employee exist in the system
			// with these code
			if (entity.getContentLength() != 0) {
				// stream reader object
				Reader employeeReader = new InputStreamReader(response
						.getEntity().getContent());
				// create a buffer to fill if from reader
				char[] buffer = new char[(int) response.getEntity()
						.getContentLength()];
				// fill the buffer by the help of reader
				employeeReader.read(buffer);
				// close the reader streams
				employeeReader.close();

				// for the employee json object
				//String s = new String(buffer);

				emp = new JSONObject(new String(buffer));
			}

		} catch (Exception e) {
			// any exception show the error layout

			// return "Error1: " + e.getMessage() + e.getLocalizedMessage();

		}
	}

}
