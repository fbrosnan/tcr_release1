package com.theclubreferee.tcr_android.timers;

public class TimersRecord {
	private String timeKickOff;
	private String matchTime;
	private String firstHalf;
	private String halfTime;
	private String secondHalf;
	private String timeOff;
	private String stopages1;
	private String stopages2;
	
	private int firstHalfExtraTime;
	private int secondHalfExtraTime;
	
	
	public TimersRecord(){
		timeKickOff = "00:00";
		matchTime   = "00:00";
		firstHalf   = "00:00";
		halfTime    = "00:00";
		secondHalf  = "00:00";
		timeOff     = "00:00";
		stopages1   = "00:00";
		stopages2   = "00:00";
	}
	
	
	
	
	public String getTimeKickOff() {
		return timeKickOff;
	}
	
	public void setTimeKickOff(String timeKickOff) {
		this.timeKickOff = timeKickOff;
	}

	public String getMatchTime() {
		return matchTime;
	}

	public void setMatchTime(String matchTime) {
		this.matchTime = matchTime;
	}

	public String getFirstHalf() {
		return firstHalf;
	}

	public void setFirstHalf(String firstHalf) {
		this.firstHalf = firstHalf;
	}

	public String getHalfTime() {
		return halfTime;
	}

	public void setHalfTime(String halfTime) {
		this.halfTime = halfTime;
	}

	public String getSecondHalf() {
		return secondHalf;
	}

	public void setSecondHalf(String secondHalf) {
		this.secondHalf = secondHalf;
	}

	public String getTimeOff() {
		return timeOff;
	}

	public void setTimeOff(String timeOff) {
		this.timeOff = timeOff;
	}

	public String getStopages2() {
		return stopages2;
	}

	public void setStopages2(String stopages2) {
		this.stopages2 = stopages2;
	}




	public int getFirstHalfExtraTime() {
		return firstHalfExtraTime;
	}




	public void setFirstHalfExtraTime(int firstHalfExtraTime) {
		this.firstHalfExtraTime = firstHalfExtraTime;
	}




	public int getSecondHalfExtraTime() {
		return secondHalfExtraTime;
	}




	public void setSecondHalfExtraTime(int secondHalfExtraTime) {
		this.secondHalfExtraTime = secondHalfExtraTime;
	}
	
	
	
	
	
	
}
