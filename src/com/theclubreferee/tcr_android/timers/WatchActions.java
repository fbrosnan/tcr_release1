package com.theclubreferee.tcr_android.timers;

public abstract class WatchActions {
	protected StopWatch stopWatch;
	
	public void setStopWatch(StopWatch stopWatch){
		this.stopWatch = stopWatch;
	}
	
	public abstract void performTickAction();
	
	public abstract void performStopAction();
	
	public abstract void performPauseAction();
	
	public abstract void performStartAction();
}
