package com.theclubreferee.tcr_android.timers;

import java.util.Date;

public class StopWatchController {
	
	//TODO These timers would be created by a factory method based on game type.
	StopWatch firstHalf;
	StopWatch firstHalfExtraTime;
	StopWatch halfTime;
	StopWatch secondHalf;
	StopWatch secondHalfExtraTime;
	StopWatch currentWatch;
	
	Date      startTime;
	
	int       playDuration;
	
	//TODO These constants decide the duration of all game phases.
	//TODO >Again these will come from a game database that lists the play durations
	//TODO: Real GAA Times
	//public static final long PLAY_DURATION     = 2100L; //35mins in secs
	//public static final long HALFTIME_DURATION = 900L;  //15mins in secs. Note sometimes this is just 10 mins
	//TODO : Development Time Lengths.
	//public static final long PLAY_DURATION     = 60L;  //5mins in secs
	//public static final long HALFTIME_DURATION = 60L;   //1mins in secs
	
	/*
	 * start the game.
	 */
	public void startGame(WatchActions firstHalfActions, 
			              WatchActions firstHalfExtraTimeActions,
			              WatchActions halfTimeActions,
			              WatchActions secondHalfActions,
			              WatchActions secondHalfExtraTimeActions,
			              int          playDuration){
		//TODO : Why do I need to add 1 below. Testing did not indicate this. Investigate.
		this.firstHalf = new StopWatch(playDuration/2,"FirstHalf",firstHalfActions,false);
		this.firstHalfExtraTime = new StopWatch(0,"FirstHalfExtraTime",firstHalfExtraTimeActions,true);
		this.halfTime = new StopWatch(0,"HalfTime",halfTimeActions,true);
		this.secondHalf = new StopWatch(playDuration/2,"SecondHalf",secondHalfActions,false);
		this.secondHalfExtraTime = new StopWatch(0,"SecondHalfExtraTime",secondHalfExtraTimeActions,true);
		
		startTime = new Date();
		currentWatch = firstHalf;
		currentWatch.startWatch();
	}
	
	/*
	 * bring the current phase to an end.
	 * or pause the game.
	 */
	public void stopGame(){
		/*
		if (currentWatch == firstHalf || currentWatch == secondHalf || currentWatch == halfTime){
			currentWatch.pauseWatch();
		}
		else if (currentWatch == firstHalfExtraTime || currentWatch == secondHalfExtraTime){
			currentWatch.stopWatch();
		}
		*/
		currentWatch.stopWatch();
	}
	
	public void restartGame(){
		currentWatch.startWatch();
	}
	
	/*
	 * change to the next stopwatch in the sequence.
	 */
	public void changeWatch(){
		if (currentWatch == firstHalf){
			currentWatch = firstHalfExtraTime;
		}
		else if (currentWatch == firstHalfExtraTime){
			currentWatch = halfTime;
		}
		else if (currentWatch == halfTime){
			currentWatch = secondHalf;
		}
		else if (currentWatch == secondHalf){
			currentWatch = secondHalfExtraTime;
		}
	}
	
	public int getElapsedTime(){
		int elapsedTime = 0;
		if (currentWatch == firstHalf){
			elapsedTime = currentWatch.getElapsedTime();
		}
		else if (currentWatch == firstHalfExtraTime){
			elapsedTime = firstHalf.getElapsedTime() + currentWatch.getElapsedTime();
		}
		else if (currentWatch == halfTime){
			elapsedTime = firstHalf.getElapsedTime() + firstHalfExtraTime.getElapsedTime();
		}
		else if (currentWatch == secondHalf){
			elapsedTime = firstHalf.getElapsedTime() +currentWatch.getElapsedTime();
		}
		else if (currentWatch == secondHalfExtraTime){
			elapsedTime = firstHalf.getElapsedTime() +secondHalf.getElapsedTime()+currentWatch.getElapsedTime();
		}
		return elapsedTime;
	}

	public boolean isMatchStart() {
		return (currentWatch == firstHalf && currentWatch.getElapsedTime() == 0);
	}

	public boolean isFirstHalfFinished() {
		// TODO Handle finish half with no extra time
		return (currentWatch == firstHalfExtraTime);
	}

	public boolean isHalfTime() {
		return (currentWatch == halfTime);
	}

	public boolean isSecondHalfFinished() {
		// TODO Handle second half with no extra time.
		return (currentWatch == secondHalfExtraTime);
	}
	
	public Date getStartTime(){
		return startTime;
	}
	
	public StopWatch getCurrentWatch(){
		return currentWatch;
	}

	
}
