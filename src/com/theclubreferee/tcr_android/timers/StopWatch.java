package com.theclubreferee.tcr_android.timers;

/**
 * @author francisbrosnan
 * 
 * StopWatch in charge of timing a phase of the game.
 * Mayby of fixed duration like first half or unlimited 
 * like injury time.
 * 
 */
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


import com.theclubreferee.tcr_android.Constants;

public class StopWatch {
	private static final int POOL_SIZE = 1;
	int                     elapsedTime; //in seconds.
	int                     timeToStop;  //in seconds.
	long                     startTime;   //in milliseconds.
	int                     accumulatedTime; //in seconds used to store the time if paused.
	String                   id;
	ScheduledFuture<?>       tickFuture;
	ScheduledFuture<?>       stopFuture;
	ScheduledExecutorService fScheduler;
	WatchActions             watchActions;
	boolean                  runIndefinitly; // runs until the user stops the watch.
	
	
	public int getElapsedTime(){
		return elapsedTime;
	}
	
	public String getId(){
		return id;
	}
	
	public boolean isIndefinite(){
		return runIndefinitly;
	}
	
	public StopWatch(int StopTimeInSeconds, String id, WatchActions watchActions, boolean runIndefinitly){
		fScheduler = Executors.newScheduledThreadPool(POOL_SIZE);
		timeToStop = StopTimeInSeconds;
		this.id = id;
		this.watchActions = watchActions; 
		if (this.watchActions != null)
			this.watchActions.setStopWatch(this);
		this.runIndefinitly = runIndefinitly;
	}
	
	public void startWatch(){
		this.startTime = System.currentTimeMillis();
		Tick tickAction = new Tick();
		//TODO: may require readjustment for pause.
		tickFuture = fScheduler.scheduleWithFixedDelay(
			      tickAction, 0, 1 , TimeUnit.SECONDS
			    );
		
		if (!runIndefinitly){
			Runnable stopAction = new Stop(tickFuture);
			stopFuture = fScheduler.schedule(stopAction, timeToStop - accumulatedTime , TimeUnit.SECONDS);
			//String s = String.format(Constants.STOPPING, timeToStop-accumulatedTime);
			//Log.i(Constants.LOG_TIMERS,s);
		}
		watchActions.performStartAction();
	}
	
	public void pauseWatch(){
		//TODO: may require readjustment for pause.
		//time was done like this so tick counts would be 
		//consistent.
		accumulatedTime = elapsedTime+1;
		tickFuture.cancel(false);
		if (!runIndefinitly){
			stopFuture.cancel(false);
		}
		watchActions.performPauseAction();
	}
	
	public void stopWatch(){
		//TODO Implement so this stops the watch.
		elapsedTime = (int) (((System.currentTimeMillis() - startTime) / 1000) + accumulatedTime);
		tickFuture.cancel(false);
	    /* 
	    Note that this Task also performs cleanup, by asking the 
	    scheduler to shutdown gracefully. 
		*/
		//fScheduler.shutdown();
		watchActions.performStopAction();
	}
	
	
	private class Tick implements Runnable {

		@Override
		public void run() {
			elapsedTime = (int) (((System.currentTimeMillis() - startTime) / 1000) + accumulatedTime);
			watchActions.performTickAction();
		}
		
	}
	
	private class Stop implements Runnable {

		ScheduledFuture<?> tickFuture;
		
		public Stop(ScheduledFuture<?> tickFuture){
			this.tickFuture = tickFuture;
		}
		
		@Override
		public void run() {
		
			elapsedTime = (int) (((System.currentTimeMillis() - startTime) / 1000) + accumulatedTime);
			tickFuture.cancel(false);
		      /* 
		       Note that this Task also performs cleanup, by asking the 
		       scheduler to shutdown gracefully. 
		      */
			//fScheduler.shutdown();
			watchActions.performStopAction();
		}
		
	}
}
