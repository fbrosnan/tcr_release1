package com.theclubreferee.tcr_android;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.UUID;

import com.theclubreferee.tcr_android.model.MatchController;
import com.theclubreferee.tcr_android.model.Persistable;
import com.theclubreferee.tcr_android.tweets.MatchEvent_AddNew;


public class MatchEvent extends Observable implements Serializable, Persistable {

	static final long serialVersionUID = -942926557033590011L;
	
	transient MatchEvent_AddNew stn;

	private MatchController matchController;
	
	public MatchEvent() {
		evGuid = UUID.randomUUID();
		this.matchController=MatchController.getInstance();
	}

	// example:
	// http://javarevisited.blogspot.ie/2011/08/enum-in-java-example-tutorial.html
	// Ray - Added enum of EventTypes + Base Values
	public enum EventTypes {
		GaaMatchAnnounced(1000), GaaPostPoned(1001), GaaCancelled(1002), GaaGoal(1030), GaaPoint(1031), SubstitutionTacticalOn(1040), SubstitutionTacticalOff(1041), SubstitutionBloodOn(
				1042), SubstitutionBloodOff(1043), GaaStart1stHalf(1051), GaaEnd1stHalf(1052), GaaStartHalfTime(1070), GaaEndHalfTime(1071), GaaStart2ndHalf(1053), GaaEnd2ndhalf(
				1054), ClearCard(1100), RedCard(1101), YellowCard(1102), BlackCard(1103), WhiteCard(1104);

		private int value;

		private EventTypes(int value) {
			this.value = value;
		}
		
		public int getValue(){
			return value;
		}

	}

	// int matchID;
	public int	evId;
	public UUID evGuid;
	public UUID playerId;
	public int  playerNumber;
	public Date evMatchTime;
	public Date evMatchRealTime; // don't know if this is needed.
	public Date evOverTime;
	public UUID teamGuid; // used on device when no internet found
	public int teamId; // used after internet trip is made
	public String teamName;
	public long evMatchTimeStamp; //the match time.
	public long evMatchTimeStampExtra; //extra time.
	
	
	
	

	public Boolean UploadSuccess = false; // will be set if a roundtrip to the server is
									// made, otherwise will attempt to upload.
	
	
	private int _goals;
	public void setGoals(int Goals){
		_goals = Goals;
	}
	
	private int _points;
	public void setPoints(int Points){
		_points = Points;
	}
	
	private EventTypes _evType;
		public void setEventType(EventTypes et){
			_evType = et;
			
		}
		public EventTypes getEventType(){
			return _evType;
		}

	// Send the event to the server
	public void SendEvent() {
		if (UploadSuccess == false) {
			stn = new MatchEvent_AddNew();
			stn.evGuid = evGuid;
			stn.matchId= matchController.getMatchID();
			stn.evType = getEventType();
			stn.playerId = UUID.fromString("00000000-0000-0000-0000-000000000000");  //TODO actualise player id.
			Date d = new Date();
			stn.evMatchTime = evMatchTime;
			stn.evMatchRealTime = evMatchRealTime;
			stn.evOverTime = evOverTime;
			stn.teamId = this.teamId;
			stn.playerNumber = Globals.CurrentEvent.playerNumber;
			
			Thread t = new Thread(stn);
			t.start();
			//Add a comment to this line
			try {
				t.join(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace()  ;
			}
			
			
		}
	}
	
	// Create a method to update the Observerable's flag to true for changes and
	// notify the observers to check for a change. These are also a part of the
	// secret sauce that makes Observers and Observables communicate//		String gaaScore = String.valueOf(Globals.AllMatchEvents.getGaaGoals()) + "-";

	////////////////^^debug^^/////////////////////////

	// predictably.
	public void triggerObservers() {
	
			setChanged();
			notifyObservers();
		}
	
	//return formatted string.
	public String toString(){
		String returnStr = "";
			
		String timeStamp;
		timeStamp = getTimeStamp();
			
			
		String scoreFormat = "%S  %S scored by #%d for %S";
		String cardFormat  = "%S  %S card shown to #%d for %S";
		switch (_evType) {
		case GaaGoal:returnStr = String.format(scoreFormat,timeStamp, "Goal",playerNumber, teamName);
						 break;
		case GaaPoint:returnStr = String.format(scoreFormat, timeStamp,"Point",playerNumber, teamName);
			 			 break;
		case RedCard:returnStr = String.format(cardFormat, timeStamp,"Red",playerNumber, teamName);
			  			 break;
		case YellowCard:returnStr = String.format(cardFormat, timeStamp,"Yellow",playerNumber,teamName);
				 		 break;
		case BlackCard:returnStr = String.format(cardFormat, timeStamp, "Black",playerNumber, teamName);
				 		 break;
		default:	
		}
		return returnStr;
	}
	
	
	public String getTimeStamp() {
		String timeStamp;
		if (evMatchTimeStampExtra == 0){
			timeStamp = Globals.convertElapsedTimeToMatchTime(evMatchTimeStamp);
		}
		else {
			timeStamp = Globals.convertElapsedTimeToMatchTimeExtraTime(evMatchTimeStamp, evMatchTimeStampExtra);
		}
		return timeStamp;
	}
	
	public void copy(MatchEvent event){
		this.stn = event.stn;
		// int matchID;
		this.evId = event.evId;
		this.evGuid = event.evGuid;
		this.playerId = event.playerId;
		this.playerNumber = event.playerNumber;
		this.evMatchTime = event.evMatchTime;
		this.evMatchRealTime = event.evMatchRealTime; 
		this.evOverTime = event.evOverTime;
		this.teamGuid = event.teamGuid; 
		this.teamId = event.teamId; 
		this.evMatchTimeStamp = event.evMatchTimeStamp; 
		this.evMatchTimeStampExtra = event.evMatchTimeStampExtra;
		this.teamName=event.teamName;
		this._evType = event._evType;
		
		
	}
	
	/**
	 * save an event using serialization.
	 */
	public boolean saveToLocalStorage(){
		boolean success = false;
		File baseDir = Globals.BaseDir;
		if (baseDir != null && baseDir.exists()){
			FileOutputStream fos = null;
			ObjectOutputStream out = null;
			try {
				
				File file = new File(baseDir,"Events.ser");
				fos = new FileOutputStream(file,true);
				out = new ObjectOutputStream(fos);
				out.writeObject(this);
				
				out.close();
				success = true;
				
			} catch (Exception ex) {
				ex.printStackTrace();
				
			}
		}
		else 
		{
			
		}
		return success;
	}
	
	/**
	 * load an event back up using serialization.
	 */
	public boolean load(){
		//TODO
		return true;
	}
	
	/**
	 * clear the file from memory
	 */
	public static void delete(){
		File baseDir = Globals.BaseDir;
		File f = new File(baseDir,"Events.ser");
		FileChannel outChan;
		try {
			outChan = new FileOutputStream(f, true).getChannel();
			outChan.truncate(0);
		    outChan.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}
	
	public static boolean isEventAlreadyThere(MatchEvent ev){
		for (MatchEvent event : Globals.MatchReport.getRecords()){
			if (event.evGuid == ev.evGuid){
				return true;
			}
		}
		return false;
	}
	@Override
	public boolean saveToNetwork(Serializable object) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean saveToMemory(Serializable object) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public List<? extends Serializable> getListOfPersistables() {
		// TODO Auto-generated method stub
		return null;
	}

}
