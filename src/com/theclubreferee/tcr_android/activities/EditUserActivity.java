package com.theclubreferee.tcr_android.activities;

import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.R;
import com.theclubreferee.tcr_android.WcfConverter;
import com.theclubreferee.tcr_android.model.Referee;
import com.theclubreferee.tcr_android.model.Referees;
import com.theclubreferee.tcr_android.model.SaveType;
import com.theclubreferee.tcr_android.tweets.UserName_AddNew;

/**
 * 
 * @author francisbrosnan
 * This is the activity in charge of adding a new user (referee) to the system.
 */
public class EditUserActivity extends Activity implements OnClickListener, Observer {

	private EditText editTextFirstName;
	private EditText editTextLastName;
	private EditText editTextUserName;
	private CheckBox checkBoxDefault;
	private Button   saveTeamButton;
	private Button   doneButton;
	private Referees currentReferees;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Referees.getInstance().addObserver(this);
		setContentView(R.layout.activity_new_user);
		
		editTextFirstName 	= (EditText) findViewById(R.id.editUserFirstNameText);
		editTextLastName 	= (EditText) findViewById(R.id.editUserSecondNameText);
		editTextUserName 	= (EditText) findViewById(R.id.editUserNameText);
		checkBoxDefault  	= (CheckBox) findViewById(R.id.defaultUserNameCheck);
		saveTeamButton = (Button) findViewById(R.id.saveUserButton);
		saveTeamButton.setOnClickListener(this);
		doneButton = (Button)findViewById(R.id.doneUserButton);
		doneButton.setOnClickListener(this);
		
		
		//load up the current teams.
		currentReferees = Referees.getInstance();
		
		//TODO:May not needed?
		currentReferees.load();
		
	}

	@Override
	public void onClick(View v) {
		if (v == saveTeamButton){
			//currentReferees.getAllReferees().clear();
			String firstName = editTextFirstName.getText().toString();
			String lastName  = editTextLastName.getText().toString();
			String userName  = editTextUserName.getText().toString();
			
			if (!firstName.isEmpty() && !lastName.isEmpty()){
				
				if (Referees.getInstance().refereeAlreadyExists(firstName, lastName, userName)){
					Toast.makeText(getApplicationContext(), "Referee Already Exists", Toast.LENGTH_SHORT).show();
					return;
				}
				Referee myReferee = new Referee(firstName,lastName, userName,checkBoxDefault.isChecked());
				
				
				
				
			}
			else {
				Toast.makeText(getApplicationContext(), "User Name Fields Required", Toast.LENGTH_SHORT).show();
			}
			
		}
		if (v == doneButton){
			//out of here !
			finish();
		}
	}

	@Override
	public void update(Observable arg0, final Object arg1) {
		// TODO Auto-generated method stub
		this.runOnUiThread(
				new Runnable(){
					public void run(){
				
		SaveType type= (SaveType)arg1;
		
		Toast.makeText(getApplicationContext(), type.getItem() +( type.wasSuccessful()==false?" WERE NOT ": " WERE ")+ "Saved to: " + type.getStorageType(), Toast.LENGTH_SHORT).show();
					}	});
		
	}
}
