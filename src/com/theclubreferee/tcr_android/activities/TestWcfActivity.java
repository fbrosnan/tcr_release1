//Using this class for testing WCF functionality
package com.theclubreferee.tcr_android.activities;

import java.util.Date;
import java.util.UUID;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.MatchEvent;
import com.theclubreferee.tcr_android.R;
import com.theclubreferee.tcr_android.tweets.MatchEvent_AddNew;

public class TestWcfActivity extends Activity {

	private EditText evEmployeeId;
	private TextView tvName;
	private TextView tvAddress;
	private TextView tvBloodGroup;
	private LinearLayout linearLayoutEmp;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_wcf);

		evEmployeeId = (EditText) findViewById(R.id.evEmployeeCode);

		tvName = (TextView) findViewById(R.id.tvEmployeeName);

		tvAddress = (TextView) findViewById(R.id.tvAddress);
		tvBloodGroup = (TextView) findViewById(R.id.tvBloodGroup);

		linearLayoutEmp = (LinearLayout) findViewById(R.id.lLayoutEmployeeInfo);
		linearLayoutEmp.setVisibility(View.VISIBLE);

	}

	public void onSearchClick(View view) {
		try {

			//TeamName_Update stn = new TeamName_Update();
			//			stn.TeamId = 10003;
//			stn.TeamName = "%20Updates";
//			stn.Description = "Android%20Emu";
//			stn.TeamGuid = UUID.randomUUID();
			
			MatchEvent_AddNew stn = new MatchEvent_AddNew();
			stn.evGuid = UUID.randomUUID();
			stn.matchId= 10000;
			stn.evType = MatchEvent.EventTypes.GaaGoal;
			stn.playerId = UUID.randomUUID();
			Date d = new Date();
			stn.evMatchTime = d;
			stn.evMatchRealTime = d;
			stn.evOverTime = d;
			stn.teamId = 10000;
					
			
			Thread t = new Thread(stn);
			t.start();
			 
			t.join();

			tvBloodGroup.setText("Response: " + Globals.MatchEventResponse);
			System.out.println(Globals.MatchEventResponse);

		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(TestWcfActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

		}

	}
}
