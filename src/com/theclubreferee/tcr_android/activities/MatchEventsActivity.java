package com.theclubreferee.tcr_android.activities;

import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.MatchEvent;
import com.theclubreferee.tcr_android.MatchEvent.EventTypes;
import com.theclubreferee.tcr_android.R;
import com.theclubreferee.tcr_android.model.IGAAPlayer;
import com.theclubreferee.tcr_android.model.IPlayer;
import com.theclubreferee.tcr_android.model.MatchController;

public class MatchEventsActivity extends Activity implements OnItemClickListener {

	private Button btnGoal;
	private Button btnPoint;
	private Button btnYellow;
	private Button btnRed;
	private Button btnBlack;
	private ListView listViewPlayerNumbers;
	private MatchController controller;
	
	
	enum EventType { GAA_YC, GAA_BC, GAA_RC, GAA_GOAL, GAA_POINT };
	private EventType eventType=null;
	private boolean homeTeam=false;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_events);

		btnGoal = (Button) findViewById(R.id.btnGoal);
		btnPoint = (Button) findViewById(R.id.btnPoint);
		btnYellow = (Button) findViewById(R.id.btnYellow);
		btnRed = (Button) findViewById(R.id.btnRed);
		btnBlack = (Button) findViewById(R.id.btnBlack);
		
		listViewPlayerNumbers = (ListView)findViewById(R.id.listViewPlayerNumbers);

		controller = MatchController.getInstance();
		homeTeam=this.getIntent().getStringExtra("whichteam").equals("HOME");
		
		
		//arrayAdapter = new ArrayAdapter<String>(this, R.layout.playernumber_listitem,R.id.playerNumberItem, numbersArray);
		//arrayAdapter = new ArrayAdapter<String>(this,
        //        android.R.layout.simple_list_item_multiple_choice, numbersArray);
		List<IPlayer> players = homeTeam?controller.getHomeTeamPlayers():controller.getAwayTeamPlayers(); 
		ArrayAdapter<IPlayer> arrayAdapter=new ArrayAdapter<IPlayer>(
	            this,android.R.layout.simple_list_item_multiple_choice, players){

	        @Override
	        public View getView(int position, View convertView,
	                ViewGroup parent) {
	            View view =super.getView(position, convertView, parent);

	            TextView textView=(TextView) view.findViewById(android.R.id.text1);

	            /*YOUR CHOICE OF COLOR*/
	            textView.setTextColor(Color.WHITE);
	            textView.setTextSize(25);
	            IPlayer player = this.getItem(position);
	            if (player instanceof IGAAPlayer)
	            {
	            	IGAAPlayer gaaPlayer = (IGAAPlayer)(player);
	            	if (gaaPlayer.getGoalCount()>0)
	            	{
	            		textView.setBackgroundColor(Color.GREEN);
	            	}
	            	else if (gaaPlayer.getPointCount()>0)
	            	{
	            		textView.setBackgroundColor(Color.BLUE);
	            	}
	            	
	            	if (gaaPlayer.getBlackCardCount() > 0)
	            	{
	            		textView.setBackgroundColor(Color.WHITE);
	            		textView.setTextColor(Color.BLACK);
	            	}
	            	if (gaaPlayer.getYellowCardCount() >0)
	            	{
	            		textView.setBackgroundColor(Color.YELLOW);
	            		textView.setTextColor(Color.BLACK);
	            	}
	            	if (gaaPlayer.getRedCardCount() >0)
	            	{
	            		textView.setBackgroundColor(Color.RED);
	            		textView.setTextColor(Color.BLACK);
	            	}
	            	
	            }
	            

	            return view;
	        }
	    };
	    
	    
		
		
		
		
		// By using setAdapter method, you plugged the ListView with adapter
		listViewPlayerNumbers.setAdapter(arrayAdapter);
		listViewPlayerNumbers.setOnItemClickListener(this);
		
		listViewPlayerNumbers.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		//TODO: Find out right way to do this.
		listViewPlayerNumbers.setSelection(0);
		listViewPlayerNumbers.setItemChecked(0, true);

		//By default the first player is selected so we set up the event with the number of the first player number
		Globals.CurrentEvent.playerNumber=((IPlayer)(listViewPlayerNumbers.getItemAtPosition(0))).getNumber();

	}



	public void btnGaaGoalScored_Click(View view) {
		Globals.CurrentEvent.setEventType(EventTypes.GaaGoal); // GAA Goal
		Globals.CurrentEvent.teamName=homeTeam?controller.getHomeTeamName():controller.getAwayTeamName();
		clearEventButtonBgColours();
		btnGoal.setBackgroundResource(R.drawable.eventbuttonon);
		eventType=EventType.GAA_GOAL;
	}

	

	public void btnGaaPointScored_Click(View view) {
		Globals.CurrentEvent.setEventType(EventTypes.GaaPoint); //
		Globals.CurrentEvent.teamName=homeTeam?controller.getHomeTeamName():controller.getAwayTeamName();
		clearEventButtonBgColours();
		btnPoint.setBackgroundResource(R.drawable.eventbuttonon);
		eventType=EventType.GAA_POINT;
	}

	public void btnGaaYellowCard_Click(View view) {
		Globals.CurrentEvent.setEventType(EventTypes.YellowCard); //
		Globals.CurrentEvent.teamName=homeTeam?controller.getHomeTeamName():controller.getAwayTeamName();
		clearEventButtonBgColours();
		btnYellow.setBackgroundResource(R.drawable.eventbuttonyellowon);
		btnYellow.setTextColor(Color.BLACK);
		eventType=EventType.GAA_YC;
	}

	public void btnGaaRedCard_Click(View view) {
		Globals.CurrentEvent.setEventType(EventTypes.RedCard); // 
		Globals.CurrentEvent.teamName=homeTeam?controller.getHomeTeamName():controller.getAwayTeamName();
		clearEventButtonBgColours();
		btnRed.setBackgroundResource(R.drawable.eventbuttonredon);
		eventType=EventType.GAA_RC;
	}

	
	public void btnGaaBlackCard_Click(View view) {
		Globals.CurrentEvent.setEventType(EventTypes.BlackCard); 
		Globals.CurrentEvent.teamName=homeTeam?controller.getHomeTeamName():controller.getAwayTeamName();
		clearEventButtonBgColours();
		btnBlack.setBackgroundResource(R.drawable.eventbuttonblackon);
		btnBlack.setTextColor(Color.BLACK);
		eventType=EventType.GAA_BC;
	}
	
	private void clearEventButtonBgColours(){
		btnGoal.setBackgroundResource(R.drawable.eventbuttonoff);
		btnPoint.setBackgroundResource(R.drawable.eventbuttonoff);
		btnYellow.setBackgroundResource(R.drawable.eventbuttonsmalloff);
		btnYellow.setTextColor(Color.WHITE);
		btnRed.setBackgroundResource(R.drawable.eventbuttonsmalloff);
		btnBlack.setBackgroundResource(R.drawable.eventbuttonsmalloff);
		btnBlack.setTextColor(Color.WHITE);
	}
	
	public void btnSaveEvent_Click(View view) {
	 
		if (eventType != null) {
			
				switch (eventType) {
				case GAA_YC:
					if (homeTeam)
					{
						controller.incrementYellowCardCountForHomePlayer(Globals.CurrentEvent.playerNumber);
					}
					else
					{
						controller.incrementYellowCardCountForAwayPlayer(Globals.CurrentEvent.playerNumber);
					}
					break;
				case GAA_RC:
					if (homeTeam)
					{
						controller.incrementRedCardCountForHomePlayer(Globals.CurrentEvent.playerNumber);
					}
					else
					{
						controller.incrementRedCardCountForAwayPlayer(Globals.CurrentEvent.playerNumber);
					}
					break;
				case GAA_BC:
					if (homeTeam)
					{
						controller.incrementBlackCardCountForHomePlayer(Globals.CurrentEvent.playerNumber);
					}
					else
					{
						controller.incrementBlackCardCountForAwayPlayer(Globals.CurrentEvent.playerNumber);
					}
					break;
				case GAA_GOAL:
					if (homeTeam)
					{
						controller.incrementGoalCountForHomePlayer(Globals.CurrentEvent.playerNumber);
					}
					else
					{
						controller.incrementGoalCountForAwayPlayer(Globals.CurrentEvent.playerNumber);
					}
					break;
				case GAA_POINT:
					if (homeTeam)
					{
						controller.incrementPointCountForHomePlayer(Globals.CurrentEvent.playerNumber);
					}
					else
					{
						controller.incrementPointCountForAwayPlayer(Globals.CurrentEvent.playerNumber);
					}
					break;

				}
		Globals.AllMatchEvents.add(Globals.CurrentEvent);
		if (Globals.isServerUp)
			Globals.AllMatchEvents.TransmitEvents();

		//Take a copy of the event for the match report.
		if (Globals.CurrentEvent.getEventType() == EventTypes.GaaGoal 	||
			Globals.CurrentEvent.getEventType() == EventTypes.GaaPoint	||
			Globals.CurrentEvent.getEventType() == EventTypes.BlackCard ||
			Globals.CurrentEvent.getEventType() == EventTypes.RedCard   ||
			Globals.CurrentEvent.getEventType() == EventTypes.YellowCard) {
					
			MatchEvent event = new MatchEvent();
			event.copy(Globals.CurrentEvent);
			Globals.MatchReport.getRecords().add(event);
			
			boolean saveEvent = event.saveToLocalStorage();
			if (!saveEvent){
				Log.d("TCR","Event "+ event.getEventType().toString() +" Failed to save");
			}
			
		}
		
		
		
		//create a new report record on the phone.
		//MatchReportRecord record = new MatchReportRecord();
		//record.setMatchEventType(Globals.CurrentEvent.getEventType());
		//TODO: Need correct player number.
		//record.setPlayNumber((int)(Math.random()*15+1));
		
		//Teams teams = new Teams();
		//teams.load();
		
		//record.setTeamName(teams.getTeamByGuid(Globals.CurrentEvent.teamGuid).getTeamName());
		//record.setTimeStamp(Globals.CurrentEvent.evMatchTimeStamp,Globals.CurrentEvent.evMatchTimeStampExtra);
		//MatchEvent event = new MatchEvent();
		//event.copy(Globals.CurrentEvent);
		
		//Globals.MatchReport.getRecords().add(event);
		
		setResult(RESULT_OK, getIntent());
		}
		finish();
		
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_events, menu);
		return true;
	}



	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Globals.CurrentEvent.playerNumber = ((IPlayer)(parent.getItemAtPosition(position))).getNumber();
		
	}


}
