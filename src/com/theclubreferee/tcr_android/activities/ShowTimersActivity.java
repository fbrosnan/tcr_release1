/**
 * 
 */
package com.theclubreferee.tcr_android.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.R;

/**
 * @author francisbrosnan
 *
 */
public class ShowTimersActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_showtimers);
		
		TextView textViewKickOff = (TextView)findViewById(R.id.textViewKickOff);
		TextView textViewMatchTime = (TextView)findViewById(R.id.textViewMatchTime);
		TextView textViewFirstHalf = (TextView)findViewById(R.id.textViewFirstHalf);
		TextView textViewHalfTime = (TextView)findViewById(R.id.textViewHalfTime);
		TextView textViewSecondHalf = (TextView)findViewById(R.id.textViewSecondHalf);
		
		textViewKickOff.setText(Globals.TimersRecord.getTimeKickOff());
		textViewMatchTime.setText(Globals.TimersRecord.getMatchTime());
		textViewFirstHalf.setText(Globals.TimersRecord.getFirstHalf());
		textViewHalfTime.setText(Globals.TimersRecord.getHalfTime());
		textViewSecondHalf.setText(Globals.TimersRecord.getSecondHalf());
		
		
	}
	
	
}
