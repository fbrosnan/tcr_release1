package com.theclubreferee.tcr_android.activities;
/**
 * This is the activity in charge of creating a new team
 */
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.theclubreferee.tcr_android.R;
import com.theclubreferee.tcr_android.model.SaveType;
import com.theclubreferee.tcr_android.model.Team;
import com.theclubreferee.tcr_android.model.Teams;


public class EditTeamActivity extends Activity implements OnClickListener, Observer {

	private EditText editTextName;
	private EditText editTextDescription;
	private Button   saveTeamButton;
	private Button   doneButton;
	private Teams    currentTeams;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Teams.getInstance().addObserver(this);
		setContentView(R.layout.activity_edit_team);
		editTextName = (EditText) findViewById(R.id.editTeamNameText);
		editTextDescription = (EditText) findViewById(R.id.editTeamDescriptionText);
		saveTeamButton = (Button) findViewById(R.id.saveTeamButton);
		saveTeamButton.setOnClickListener(this);
		doneButton = (Button)findViewById(R.id.doneTeamButton);
		doneButton.setOnClickListener(this);
		
		//load up the current teams.
		currentTeams = Teams.getInstance();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_edit_team, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		//save the new team.
		if (v == saveTeamButton){
			
			String teamName = editTextName.getText().toString();
			String teamDescription = editTextDescription.getText().toString();
			//validate the user data
			if (!teamName.isEmpty()){
				
				//save the data
				
				if (currentTeams.doesTeamExisitAlready(teamName)){
					Toast.makeText(getApplicationContext(), "Team Already Exists", Toast.LENGTH_SHORT).show();
					return;
				}
				Team myTeam = new Team(teamName, teamDescription, 0, UUID.randomUUID());
			}
			else {
				Toast.makeText(getApplicationContext(), "Team Name Field Required", Toast.LENGTH_SHORT).show();
			}	
		}
		if (v == doneButton){
			//out of here !
			finish();
		}
	}

	@Override
	public void update(Observable arg0, final Object arg1) {
		// TODO Auto-generated method stub
		this.runOnUiThread(
				new Runnable(){
					public void run(){
				
		SaveType type= (SaveType)arg1;
		
		Toast.makeText(getApplicationContext(), type.getItem() +( type.wasSuccessful()==false?" WERE NOT ": " WERE ")+ "Saved to: " + type.getStorageType(), Toast.LENGTH_SHORT).show();
					}	});
	}

}
