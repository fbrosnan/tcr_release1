package com.theclubreferee.tcr_android.activities;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.theclubreferee.tcr_android.AboutActivity;
import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.MatchEvent;
import com.theclubreferee.tcr_android.MatchEvent.EventTypes;
import com.theclubreferee.tcr_android.MatchEvents;
import com.theclubreferee.tcr_android.R;
import com.theclubreferee.tcr_android.TestWcf;
import com.theclubreferee.tcr_android.model.Match;
import com.theclubreferee.tcr_android.model.MatchController;
import com.theclubreferee.tcr_android.model.Referees;
import com.theclubreferee.tcr_android.model.Team;
import com.theclubreferee.tcr_android.model.Teams;
import com.theclubreferee.tcr_android.model.Venues;
import com.theclubreferee.tcr_android.timers.StopWatchController;
import com.theclubreferee.tcr_android.timers.TimersRecord;
import com.theclubreferee.tcr_android.timers.WatchActions;

public class MainActivity extends Activity implements Observer {

	private static final int REQUEST_FOR_SETUP = 0;
	private static final int REQUEST_FOR_HOME_TEAM = 1;
	private static final int REQUEST_FOR_AWAY_TEAM = 2;
	Button btnMainTimer;
	Button btnHomeTeam;
	Button btnAwayTeam;
	TextView tvHomeTeam;
	TextView tvAwayTeam;
	TextView tvListEvents;
	TextView tvMatchPhase;
	boolean  timerStopped; //currentState of The Timer Button.
	StopWatchController stopWatchController;
	private Teams currentTeams;
	private Venues currentVenues;
	private Referees currentReferees;
	private TextView tvHomeYellows;
	private TextView tvHomeReds;
	private TextView tvHomeBlacks;
	private TextView tvAwayYellows;
	private TextView tvAwayReds;
	private TextView tvAwayBlacks;
	private MatchController controller=null;
	
	
	private class FirstHalfActions extends WatchActions {

		@Override
		public void performTickAction() {
			// TODO Auto-generated method stub
			long elapsedTime = stopWatch.getElapsedTime();
			final int elapsedMins = (int) (elapsedTime / 60);
			final int elapsedSecs = (int) (elapsedTime % 60);
			//btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
			
			runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
	            	Globals.TimersRecord.setFirstHalf(String.format("%02d:%02d", elapsedMins,elapsedSecs));
	            	
	            }
	        });
			
			
			//btnMainTimer.setText("WUBU WUBU");
		}

		@Override
		public void performStopAction() {
			// TODO Auto-generated method stub
			
			long elapsedTime = stopWatch.getElapsedTime();
			final int elapsedMins = (int) (elapsedTime / 60);
			final int elapsedSecs = (int) (elapsedTime % 60);
			
			runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
	            	Globals.TimersRecord.setFirstHalf(String.format("%02d:%02d", elapsedMins,elapsedSecs));
	            }
	        });
			
			//Start the 1st Half Extra Time Timer.
			stopWatchController.changeWatch();
			stopWatchController.restartGame();
			timerStopped = false;
			
		}

		@Override
		public void performPauseAction() {
				runOnUiThread(new Runnable() {
					@Override
				    public void run() {
						//btnMainTimer.setBackgroundColor(Color.RED);
				    }
				});
	
		}

		@Override
		public void performStartAction() {
			
			runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	//btnMainTimer.setBackgroundColor(Color.GREEN);
	            	tvMatchPhase.setText("FirstHalf");
	            	controller.setMatchPeriod(Match.MatchPeriod.FirstHalf);
	            	invalidateOptionsMenu();
	            }
	        });
			
			Globals.isServerUp = sendWCFEvent(EventTypes.GaaStart1stHalf,stopWatchController.getStartTime(),stopWatchController.getElapsedTime(),0);
		}
		
	}
	
	private class FirstHalfExtraTimeActions extends WatchActions {

		@Override
		public void performTickAction() {
			
			// Hard code to the first half of gaa matches.
			int elapsedTime = stopWatch.getElapsedTime() + (controller.getMatchDuration()*60)/2;
			final int elapsedMins = (int) (elapsedTime / 60);
			final int elapsedSecs = (int) (elapsedTime % 60);
			//btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
						
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
					Globals.TimersRecord.setFirstHalf(String.format("%02d:%02d", elapsedMins,elapsedSecs));
				}
		    });
		}

		@Override
		public void performStopAction() {
			// TODO Auto-generated method stub
			final int extraTime = stopWatch.getElapsedTime();
			int elapsedTime = extraTime + (controller.getMatchDuration()*60)/2;
			final int elapsedMins = (int) (elapsedTime / 60);
			final int elapsedSecs = (int) (elapsedTime % 60);
			btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
						
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
					Globals.TimersRecord.setFirstHalf(String.format("%02d:%02d", elapsedMins,elapsedSecs));
					Globals.TimersRecord.setFirstHalfExtraTime(extraTime);
					//btnMainTimer.setBackgroundColor(Color.RED);
				}
		    });
			
			stopWatchController.changeWatch();
			timerStopped = true;
			if (Globals.isServerUp)
				sendWCFEvent(EventTypes.GaaEnd1stHalf,stopWatchController.getStartTime(),stopWatchController.getElapsedTime(),stopWatchController.getElapsedTime());
		}

		@Override
		public void performPauseAction() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void performStartAction() {
			
			runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	//btnMainTimer.setBackgroundColor(Color.MAGENTA);
	            	//tvMatchPhase.setText("FirstHalf InjuryTime");
	            }
	        });
		}
		
	}
	
	private class HalfTimeActions extends WatchActions {

		@Override
		public void performTickAction() {
			int elapsedTime = stopWatch.getElapsedTime();
			final int elapsedMins = (int) (elapsedTime / 60);
			final int elapsedSecs = (int) (elapsedTime % 60);
			//btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
			
			runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
	            	Globals.TimersRecord.setHalfTime((String.format("%02d:%02d", elapsedMins,elapsedSecs)));
	            }
	        });
			
		}

		@Override
		public void performStopAction() {
			int elapsedTime = stopWatch.getElapsedTime();
			final int elapsedMins = (int) (elapsedTime / 60);
			final int elapsedSecs = (int) (elapsedTime % 60);
			
			runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
	            	Globals.TimersRecord.setHalfTime((String.format("%02d:%02d", elapsedMins,elapsedSecs)));
	            	//btnMainTimer.setBackgroundColor(Color.RED);
	            }
	        });
			
			
			stopWatchController.changeWatch();
			timerStopped = true;
			//sendWCFEvent(EventTypes.GaaEndHalfTime);
		}

		@Override
		public void performPauseAction() {
			runOnUiThread(new Runnable() {
				@Override
			    public void run() {
					//btnMainTimer.setBackgroundColor(Color.RED);
			    }
			});
			
		}

		@Override
		public void performStartAction() {
			// TODO Auto-generated method stub
			
			runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	//btnMainTimer.setBackgroundColor(Color.GREEN);
	            	tvMatchPhase.setText("HalfTime");
	            	controller.setMatchPeriod(Match.MatchPeriod.HalfTime);
	            	invalidateOptionsMenu();
	            }
	        });
			//currentSettings.setMatchPeriod(Match.MatchPeriod.HalfTime);
			//sendWCFEvent(EventTypes.GaaStartHalfTime);
		}
		
	}
	
	private class SecondHalfActions extends WatchActions {

		@Override
		public void performTickAction() {
			// TODO Auto-generated method stub
			int elapsedTime = stopWatch.getElapsedTime()+(controller.getMatchDuration()*60)/2;
			final int elapsedMins = (int) (elapsedTime / 60);
			final int elapsedSecs = (int) (elapsedTime % 60);
			
			//get real time.
			int timerTime = elapsedTime-(controller.getMatchDuration()*60)/2;
			final int timerMins = (int) (timerTime / 60);
			final int timerSecs = (int) (timerTime % 60);
			
			
			//btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
						
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
					Globals.TimersRecord.setSecondHalf(String.format("%02d:%02d", timerMins,timerSecs));
				}
		    });
			
		}

		@Override
		public void performStopAction() {
			int elapsedTime = stopWatch.getElapsedTime() +(controller.getMatchDuration()*60)/2;
			final int elapsedMins = (int) (elapsedTime / 60);
			final int elapsedSecs = (int) (elapsedTime % 60);
			
			//get real time.
			int timerTime = elapsedTime-(controller.getMatchDuration()*60)/2;
			final int timerMins = (int) (timerTime / 60);
			final int timerSecs = (int) (timerTime % 60);
			
			runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
	            	Globals.TimersRecord.setSecondHalf(String.format("%02d:%02d", timerMins,timerSecs));
	            }
	        });
			
			//Start the 2nd Half Extra Time Timer.
			stopWatchController.changeWatch();
			stopWatchController.restartGame();
			timerStopped = false;
			
		}

		@Override
		public void performPauseAction() {
			runOnUiThread(new Runnable() {
				@Override
			    public void run() {
					//btnMainTimer.setBackgroundColor(Color.RED);
			    }
			});
		}

		@Override
		public void performStartAction() {
			runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	//btnMainTimer.setBackgroundColor(Color.GREEN);
	            	tvMatchPhase.setText("SecondHalf");
	            	controller.setMatchPeriod(Match.MatchPeriod.SecondHalf);
	            	invalidateOptionsMenu();
	            }
	        });
			//currentSettings.setMatchPeriod(Match.MatchPeriod.SecondHalf);
			if (Globals.isServerUp)
				sendWCFEvent(EventTypes.GaaStart2ndHalf,stopWatchController.getStartTime(),stopWatchController.getElapsedTime(),0);
		}
		
	}
	
	private class SecondHalfExtraTimeActions extends WatchActions {

		@Override
		public void performTickAction() {

			// Hard code to the first half of gaa matches.
			int elapsedTime = stopWatch.getElapsedTime() + controller.getMatchDuration()*60;
			final int elapsedMins = (int) (elapsedTime / 60);
			final int elapsedSecs = (int) (elapsedTime % 60);
			
			//get real time.
			int timerTime = elapsedTime+(controller.getMatchDuration()*60/2);
			final int timerMins = (int) (timerTime / 60);
			final int timerSecs = (int) (timerTime % 60);
			
			
			//btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
						
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
					Globals.TimersRecord.setSecondHalf(String.format("%02d:%02d", timerMins,timerSecs));
				}
		    });
			
		}

		@Override
		public void performStopAction() {
			// TODO Auto-generated method stub
			final int extraTime = stopWatch.getElapsedTime();
			
			int elapsedTime = extraTime + controller.getMatchDuration()*60;
			final int elapsedMins = (int) (elapsedTime / 60);
			final int elapsedSecs = (int) (elapsedTime % 60);
			
			//get real time.
			int timerTime = extraTime+(controller.getMatchDuration()*60/2);
			final int timerMins = (int) (timerTime / 60);
			final int timerSecs = (int) (timerTime % 60);
			
			//get full time calculation.
			int fullTime = extraTime+Globals.TimersRecord.getFirstHalfExtraTime()+controller.getMatchDuration()*60;
			final int fullTimeMins = (int) (fullTime/60);
			final int fullTimeSecs = (int) (fullTime%60);
			
									
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					btnMainTimer.setText(String.format("%02d:%02d", elapsedMins,elapsedSecs));
					Globals.TimersRecord.setSecondHalf(String.format("%02d:%02d", timerMins,timerSecs));
					Globals.TimersRecord.setSecondHalfExtraTime(extraTime);
					Globals.TimersRecord.setMatchTime(String.format("%02d:%02d", fullTimeMins,fullTimeSecs));
					//btnMainTimer.setBackgroundColor(Color.RED);
					tvMatchPhase.setText("Game Over");
					controller.setMatchPeriod(Match.MatchPeriod.MatchFinished);
					invalidateOptionsMenu();
				}
			});
									
		    timerStopped = true;
		    if (Globals.isServerUp)
		    	sendWCFEvent(EventTypes.GaaEnd2ndhalf,stopWatchController.getStartTime(),stopWatchController.getElapsedTime(),stopWatchController.getElapsedTime());
		}

		@Override
		public void performPauseAction() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void performStartAction() {
			runOnUiThread(new Runnable() {
	            @Override
	            public void run() {
	            	//btnMainTimer.setBackgroundColor(Color.MAGENTA);
	            	//tvMatchPhase.setText("SecondHalf Injury Time");
	            }
	        });
			
		}
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Load initial data
		controller = MatchController.getInstance();
		controller.addObserver(this);
	    currentTeams    = Teams.getInstance();
	    
	    currentVenues   = Venues.getInstance();
	    
	    currentReferees = Referees.getInstance();
	    
	    //TODO:Question... are these needed?
	    currentVenues.load();
	    currentTeams.load();
	    currentReferees.load();
	    
		setContentView(R.layout.activity_main);
		btnMainTimer = (Button) findViewById(R.id.btnMainTimer);
		
		btnHomeTeam = (Button) findViewById(R.id.btnHome);
		btnAwayTeam = (Button) findViewById(R.id.btnAway);
		
		tvListEvents = (TextView) findViewById(R.id.tvListEvents);
		
		tvMatchPhase = (TextView) findViewById(R.id.tvMatchPhase);
		
		tvHomeTeam = (TextView) findViewById(R.id.tvHomeTeamName);
		tvAwayTeam = (TextView) findViewById(R.id.tvAwayTeamName);
		
		tvHomeYellows = (TextView) findViewById(R.id.tvHomeYellows);
		tvHomeReds = (TextView) findViewById(R.id.tvHomeReds);
		tvHomeBlacks = (TextView) findViewById(R.id.tvHomeBlacks);
		
		tvAwayYellows = (TextView) findViewById(R.id.tvAwayYellows);
		tvAwayReds = (TextView) findViewById(R.id.tvAwayReds);
		tvAwayBlacks = (TextView) findViewById(R.id.tvAwayBlacks);
		
		
		if(Globals.AllMatchEvents == null)
			Globals.AllMatchEvents = new MatchEvents();
		
		Globals.AllMatchEvents.addObserver(this);
		
		
		Globals.BaseDir = this.getDir("TCRData", Context.MODE_PRIVATE);
		if (Globals.BaseDir.exists()==false)
		{
			Globals.BaseDir.mkdirs();
		}
		
		//TODO : This can be removed after some testing
		//Reset the match setup on startup.
		

		
		if (controller != null && !(controller.isSetUp())){
			btnMainTimer.setText("SetUp");
		}
		else if (controller == null && controller.isSetUp() && controller.getMatchPeriod() == Match.MatchPeriod.MatchNotStarted) {
			btnMainTimer.setText("Start");
		}
		
		//Reset Match Events.
		MatchEvent.delete();
		
		
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		invalidateOptionsMenu();
		Log.d("TCR","onResume");
		if (controller != null && !(controller.isSetUp())){
			btnMainTimer.setText("SetUp");
		}
		else if (controller != null && controller.isSetUp() && controller.getMatchPeriod() == Match.MatchPeriod.MatchNotStarted) {
			btnMainTimer.setText("Start");
			tvHomeTeam.setText(controller.getHomeTeamName());
			tvAwayTeam.setText(controller.getAwayTeamName());
			Globals.AllMatchEvents.triggerObservers();
		}
		
		
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		Log.d("TCR","onPause");
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		Log.d("TCR","onStop");
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		Log.d("TCR","onStart");
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.activity_main, menu);
		return true;
	}

	// //////////////////////////////////////////////////////////////////////////////
	// //////// MAIN BUTTONS /////////////////////////////////////////////////
	public void btnMainTimer_onClick(View view) {

		//Toast.makeText(MainActivity.this, "Check match state and run timer",
		//		Toast.LENGTH_LONG).show();
		if (controller != null && !controller.isSetUp()){
			Intent intent = new Intent(this, SetupGameActivity.class);
			
    		startActivity(intent);
		}
		if (controller != null && controller.isSetUp() && controller.getMatchPeriod() == Match.MatchPeriod.MatchNotStarted){
			//start the game.
			Globals.AllMatchEvents=new MatchEvents();
			stopWatchController = new StopWatchController();
			stopWatchController.startGame(new FirstHalfActions(),
					                      new FirstHalfExtraTimeActions(),
					                      new HalfTimeActions(),
					                      new SecondHalfActions(),
					                      new SecondHalfExtraTimeActions(),
					                      controller.getMatchDuration()*60); //convert to seconds
			
			
			
			
			Globals.MatchReport.getRecords().clear();
			Globals.TimersRecord = null;
			Globals.TimersRecord = new TimersRecord();
			
			Time today = new Time(Time.getCurrentTimezone());
			today.setToNow();
			
			Globals.TimersRecord.setTimeKickOff(today.format("%H:%M"));
			
			//Reset Match Events.
			MatchEvent.delete();
			
			//load up the current teams.
			currentTeams = Teams.getInstance();
			controller.setHomeTeam(tvHomeTeam.getText().toString());
			controller.setAwayTeam(tvAwayTeam.getText().toString());
		
			//Globals.AllMatchEvents = new MatchEvents(controller,this);
			Globals.AllMatchEvents.addObserver(this);
			
			
			
			
		}

	}

	public void btnHomeEventClick(View view) {

		if (isMatchOn()){
			if(Globals.AllMatchEvents == null)
				Globals.AllMatchEvents = new MatchEvents();
			//controller.setHomeTeam(currentTeams.getTeamByName(btnHomeTeam.getText().toString()));
			Calendar calendar = new GregorianCalendar();
		
			Date d1 = calendar.getTime();
		
			Date d2 = stopWatchController.getStartTime();
			calendar.setTime(d2);
			calendar.add(Calendar.SECOND, stopWatchController.getElapsedTime());
			d2 = calendar.getTime();
		
		
			Globals.CurrentEvent = Globals.AllMatchEvents.createNewEvent();
		
			//TODO: Ray set proper times.
			Date d = new Date();
			Globals.CurrentEvent.evGuid = UUID.randomUUID();
			Globals.CurrentEvent.evMatchTime = d2;
			Globals.CurrentEvent.evMatchRealTime = d1;
			//TODO: Handle Overtime right
			Globals.CurrentEvent.evOverTime = d2;
			Globals.CurrentEvent.teamGuid = controller.getHomeTeamGuid();  //UUID.fromString("00000000-7777-0000-0000-000000000000"); 
			Globals.CurrentEvent.teamId = controller.getHomeTeamID();
			if (stopWatchController.isFirstHalfFinished()){
				Globals.CurrentEvent.evMatchTimeStamp      = (controller.getMatchDuration()/2)*60;
				Globals.CurrentEvent.evMatchTimeStampExtra = stopWatchController.getCurrentWatch().getElapsedTime();
			}
			else if (stopWatchController.isSecondHalfFinished()){
				Globals.CurrentEvent.evMatchTimeStamp      = (controller.getMatchDuration())*60;
				Globals.CurrentEvent.evMatchTimeStampExtra = stopWatchController.getCurrentWatch().getElapsedTime();
			}
			else {
				Globals.CurrentEvent.evMatchTimeStamp = stopWatchController.getElapsedTime();
			}
		
			Intent intent = new Intent(this, MatchEventsActivity.class);
			intent.putExtra("whichteam", "HOME");
			startActivityForResult(intent, REQUEST_FOR_HOME_TEAM);
		}
	
		
	}

	
	

	public void btnAwayEventClick(View view) {
		if (isMatchOn()){
			if(Globals.AllMatchEvents == null)
				Globals.AllMatchEvents = new MatchEvents();
		
			Calendar calendar = new GregorianCalendar();
		
			Date d1 = calendar.getTime();
		
			Date d2 = stopWatchController.getStartTime();
			calendar.setTime(d2);
			calendar.add(Calendar.SECOND, stopWatchController.getElapsedTime());
			d2 = calendar.getTime();
		
		
			Globals.CurrentEvent = Globals.AllMatchEvents.createNewEvent();
		
			//TODO: Set up proper times for overtime.
			Date d = new Date();
			Globals.CurrentEvent.evGuid = UUID.randomUUID();
			Globals.CurrentEvent.evMatchTime = d2;
			Globals.CurrentEvent.evMatchRealTime = d1;
			Globals.CurrentEvent.evOverTime = d2;
			Globals.CurrentEvent.teamGuid = controller.getAwayTeamGuid();  //UUID.fromString("00000000-7777-0000-0000-000000000000"); 
			Globals.CurrentEvent.teamId = controller.getAwayTeamID();
			
			if (stopWatchController.isFirstHalfFinished()){
				Globals.CurrentEvent.evMatchTimeStamp      = (controller.getMatchDuration()/2)*60;
				Globals.CurrentEvent.evMatchTimeStampExtra = stopWatchController.getCurrentWatch().getElapsedTime();
			}
			else if (stopWatchController.isSecondHalfFinished()){
				Globals.CurrentEvent.evMatchTimeStamp      = (controller.getMatchDuration())*60;
				Globals.CurrentEvent.evMatchTimeStampExtra = stopWatchController.getCurrentWatch().getElapsedTime();
			}
			else {
				Globals.CurrentEvent.evMatchTimeStamp = stopWatchController.getElapsedTime();
			}
			Intent intent = new Intent(this, MatchEventsActivity.class);
			intent.putExtra("whichteam", "AWAY");
			startActivityForResult(intent, REQUEST_FOR_AWAY_TEAM);
		}
		
	}

	// //////// END MAIN BUTTONS /////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////////////////

	// //Test Buttons //////
	public void btnOpenWcfTest(View view) {
		try {
			TestWcf evs = new TestWcf(this);
			evs.ShowTestWcfActivity();
		} catch (Exception ex) {
			Toast.makeText(MainActivity.this, ex.toString(), Toast.LENGTH_LONG).show();
		}

	}

	// //End Test Buttons////

	/**
     * Event Handling for Individual menu item selected
     * Identify single menu item by it's id
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
    	Intent intent;
         
        switch (item.getItemId())
        {
        case R.id.menu_start_game:
            // Single menu item is selected do something
            // Ex: launching new activity/screen or show alert message
        	if (controller != null && controller.isSetUp() && controller.getMatchPeriod() == Match.MatchPeriod.MatchNotStarted){
    			//start the game.
    			Globals.AllMatchEvents=new MatchEvents();
    			stopWatchController = new StopWatchController();
    			stopWatchController.startGame(new FirstHalfActions(),
    					                      new FirstHalfExtraTimeActions(),
    					                      new HalfTimeActions(),
    					                      new SecondHalfActions(),
    					                      new SecondHalfExtraTimeActions(),
    					                      controller.getMatchDuration()*60); //convert to seconds
    			
    			
    			
    			
    			Globals.MatchReport.getRecords().clear();
    			Globals.TimersRecord = null;
    			Globals.TimersRecord = new TimersRecord();
    			
    			Time today = new Time(Time.getCurrentTimezone());
    			today.setToNow();
    			
    			Globals.TimersRecord.setTimeKickOff(today.format("%H:%M"));
    			
    			//Reset Match Events.
    			MatchEvent.delete();
    			
    			//load up the current teams.
    			currentTeams = Teams.getInstance();
    			controller.setHomeTeam(tvHomeTeam.getText().toString());
    			controller.setAwayTeam(tvAwayTeam.getText().toString());
    		
    			//Globals.AllMatchEvents = new MatchEvents(controller,this);
    			Globals.AllMatchEvents.addObserver(this);
    			
    			
    			
    			
    		}

            return true;
 
        case R.id.menu_setup_game:
    		if (controller != null && (!controller.isSetUp() ||
    								   controller.getMatchPeriod() == Match.MatchPeriod.MatchNotStarted ||
    								   controller.getMatchPeriod() == Match.MatchPeriod.MatchFinished)
    				){
    			intent = new Intent(this, SetupGameActivity.class);
    			startActivity(intent);
    		} else {
    			Toast.makeText(MainActivity.this,"Game In Progress",Toast.LENGTH_SHORT).show();
    		}
            return true;
            
        case R.id.menu_next_phase:
        	
        
        	//start the game.
			if (controller != null && controller.isSetUp() && controller.getMatchPeriod() == Match.MatchPeriod.MatchNotStarted){
				stopWatchController = new StopWatchController();
				stopWatchController.startGame(new FirstHalfActions(),
											  new FirstHalfExtraTimeActions(),
											  new HalfTimeActions(),
											  new SecondHalfActions(),
											  new SecondHalfExtraTimeActions(),
											  controller.getMatchDuration()*60); //convert to seconds
				Globals.MatchReport.getRecords().clear();
			}
			//stop the game timer.
			else if ((stopWatchController != null) && (stopWatchController.isFirstHalfFinished())){
				stopWatchController.stopGame();
				stopWatchController.restartGame();
			}
			else if ((stopWatchController != null) && (stopWatchController.isHalfTime())){
        		stopWatchController.stopGame();
        		stopWatchController.restartGame();
        	}
			else if ((stopWatchController != null) && (stopWatchController.isSecondHalfFinished())){
        		stopWatchController.stopGame();
        	}
        	return true;
 
        case R.id.menu_match_report:
    		 intent = new Intent(this, MatchReportActivity.class);
    		 startActivity(intent);
             return true;
 
        case R.id.menu_about:
     		 intent = new Intent(this, AboutActivity.class);
    		startActivity(intent);
            return true;
        
        case R.id.menu_edit_team:
        	intent = new Intent(this,EditTeamActivity.class);
        	startActivity(intent);
        	return true;
        case R.id.menu_edit_user:
        	intent = new Intent(this,EditUserActivity.class);
        	startActivity(intent);
        	return true;
        case R.id.menu_new_game :
        	if (controller != null && (controller.getMatchPeriod() == Match.MatchPeriod.MatchFinished
        										 || controller.getMatchPeriod() == Match.MatchPeriod.MatchNotStarted
        			)){
        		controller.resetMatch();
        		

        		btnMainTimer.setText("Set Up");
        		tvHomeTeam.setText("Home Team");
        		tvAwayTeam.setText("Away Team");
        				
        	}
        	return true;
        case R.id.menu_show_timers :
        	intent = new Intent(this,ShowTimersActivity.class);
        	startActivity(intent);
        	return true;
       
//        case R.id.menu_delete:
//            Toast.makeText(MainActivity.this, "Delete is Selected", Toast.LENGTH_SHORT).show();
//            return true;
// 
//        case R.id.menu_preferences:
//            Toast.makeText(MainActivity.this, "Preferences is Selected", Toast.LENGTH_SHORT).show();
//            return true;
 
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    public boolean onPrepareOptionsMenu (Menu menu)
    {
    	MenuItem nextPhaseMenuItem = menu.findItem(R.id.menu_next_phase);
    	MenuItem setUpMenuItem     = menu.findItem(R.id.menu_setup_game);
    	MenuItem startMenuItem     = menu.findItem(R.id.menu_start_game);
    	
    	if (controller != null &&
    		controller.isSetUp() &&
    		controller.getMatchPeriod() == Match.MatchPeriod.MatchNotStarted){
    		startMenuItem.setIcon(R.drawable.menu_start_game_enabled);
    		startMenuItem.setEnabled(true);
    	} else {
    		startMenuItem.setIcon(R.drawable.menu_start_game);
    		startMenuItem.setEnabled(false);
    	}
    	
    	if (controller != null &&
    		(controller.getMatchPeriod() == Match.MatchPeriod.FirstHalf ||
    		controller.getMatchPeriod() == Match.MatchPeriod.HalfTime  ||
    		controller.getMatchPeriod() == Match.MatchPeriod.SecondHalf)){
    		setUpMenuItem.setIcon(R.drawable.menu_setup_game);
    		setUpMenuItem.setEnabled(false);
    	} else {
    		setUpMenuItem.setIcon(R.drawable.menu_setup_game_enabled);
    		setUpMenuItem.setEnabled(true);
    	}
    	
    	
    	
    	if (controller != null && 
    		controller.isSetUp() && 
    		controller.getMatchPeriod() == Match.MatchPeriod.MatchNotStarted){
    		nextPhaseMenuItem.setTitle("Start First Half");
    	}
    	else if ((controller != null) &&
    			 (controller.getMatchPeriod() == Match.MatchPeriod.FirstHalf)){
    		nextPhaseMenuItem.setTitle("Goto Half Time");
    	}
    	else if ((controller != null) &&
   			 (controller.getMatchPeriod() == Match.MatchPeriod.HalfTime)){
    		nextPhaseMenuItem.setTitle("Start Second Half");
    	}
    	else if ((controller != null) &&
      			 (controller.getMatchPeriod() == Match.MatchPeriod.SecondHalf)){
       		nextPhaseMenuItem.setTitle("End Game");
    	}
    	else if ((controller != null) &&
     			 (controller.getMatchPeriod() == Match.MatchPeriod.MatchFinished)){
      		nextPhaseMenuItem.setTitle("Start First Half");
   	}
    	
    	return super.onPrepareOptionsMenu(menu);
    }

	@Override
	public void update(Observable observable, Object data) {
		
		Log.d("TCR","updateScore");
		
		//Update the Scores
		if (controller.getHomeTeamName()!=null)
		{
		String gaaScore = Integer.toString(controller.getHomeTeamTotalGoals());
		gaaScore += "-"+controller.getHomeTeamTotalPoints();
		btnHomeTeam.setText( gaaScore );
		tvHomeTeam.setText(controller.getHomeTeamName());
		tvHomeYellows.setText(Integer.toString(controller.getHomeTeamTotalYellowCards()));
		tvHomeReds.setText(Integer.toString(controller.getHomeTeamTotalRedCards()));
		tvHomeBlacks.setText(Integer.toString(controller.getHomeTeamTotalBlackCards()));
		}
		if (controller.getAwayTeamName()!=null)
		{

		String gaaScore = Integer.toString(controller.getAwayTeamTotalGoals());
		gaaScore += "-"+controller.getAwayTeamTotalPoints();
		btnAwayTeam.setText( gaaScore );
		tvAwayTeam.setText(controller.getAwayTeamName());
		tvAwayYellows.setText(Integer.toString(controller.getAwayTeamTotalYellowCards()));
		tvAwayReds.setText(Integer.toString(controller.getAwayTeamTotalRedCards()));
		tvAwayBlacks.setText(Integer.toString(controller.getAwayTeamTotalBlackCards()));
		}
		
		//not using the new SCORE class yet
		//btnHomeTeam.setText(currentSettings.getHomeTeam().score.getGaaScore());
		
		//Update the TeamNames
		
		
		
		
		
		
		
		
		
		
		///this is just for debugging on the main activity
		StringBuilder sb = new StringBuilder();		
		for(MatchEvent ev: Globals.AllMatchEvents.getAllMatchEvents()){
			sb.append(ev.evId + " , " + Globals.MatchEventResponse + " \n");
		}
		this.tvListEvents.setText(sb.toString());
		////////////////^^debug^^/////////////////////////

	}

	private boolean sendWCFEvent(MatchEvent.EventTypes eventType, Date startTime, long matchTime,long overTime) {
		if(Globals.AllMatchEvents == null)
			Globals.AllMatchEvents = new MatchEvents();	
		Globals.CurrentEvent =Globals.AllMatchEvents.createNewEvent();
		
		Calendar calendar = new GregorianCalendar();
		
		Date d1 = calendar.getTime();

		Date d2 = startTime;
		calendar.setTime(startTime);
		calendar.add(Calendar.SECOND, (int)matchTime);
		d2 = calendar.getTime();
		
		Date d3 = new Date();
		if (overTime != 0){
			d3 = startTime;
			calendar.setTime(startTime);
			calendar.add(Calendar.SECOND, (int)overTime);
			d3 = calendar.getTime();
		}
		
		Globals.CurrentEvent.evGuid = UUID.randomUUID();
		
		Globals.CurrentEvent.evMatchTime = d2;
		Globals.CurrentEvent.evMatchRealTime = d1;
		
		if (overTime != 0){
			Globals.CurrentEvent.evOverTime = d3;
		}
		else {
			Globals.CurrentEvent.evOverTime = d2;
		}
		
		
		//Globals.CurrentEvent.evId = ?

		Globals.CurrentEvent.setEventType(eventType);
		Globals.AllMatchEvents.add(Globals.CurrentEvent);
		Globals.AllMatchEvents.TransmitEvents();
		
		return Globals.CurrentEvent.UploadSuccess;
		
	}
	
	private boolean isMatchOn() {
		if (controller != null){
			return (controller.getMatchPeriod() == Match.MatchPeriod.FirstHalf ||
				    controller.getMatchPeriod() == Match.MatchPeriod.SecondHalf);
		}
		else {
			return false;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		/*if (requestCode==REQUEST_FOR_SETUP)
		{
			controller=null;
			controller=(Match)(data.getSerializableExtra("match"));
			
		}
		else */if (requestCode==REQUEST_FOR_HOME_TEAM || requestCode==REQUEST_FOR_AWAY_TEAM)
		{
			Team tempTeam = null;
			if (data != null) {
				tempTeam = (Team) (data.getSerializableExtra("team"));
			}
			if (tempTeam != null && controller != null) {
				if (requestCode == REQUEST_FOR_HOME_TEAM) {
					controller.setHomeTeam(tempTeam.getTeamName());
				} else if (requestCode == REQUEST_FOR_AWAY_TEAM) {
					controller.setAwayTeam(tempTeam.getTeamName());
				}
			}
		}
	}
	

}
