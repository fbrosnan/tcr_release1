package com.theclubreferee.tcr_android.activities;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.MatchEvent;
import com.theclubreferee.tcr_android.MatchEvent.EventTypes;
import com.theclubreferee.tcr_android.R;
import com.theclubreferee.tcr_android.model.MatchController;
import com.theclubreferee.tcr_android.model.Team;
import com.theclubreferee.tcr_android.model.Teams;
import com.theclubreferee.tcr_android.tweets.MatchEvent_Update;

public class MatchEventsEditActivity extends Activity {
	
	private ListView eventTypeList;
	private ListView eventTeamList;
	private EditText eventPlayerNumber;
	private EditText eventTime;
	private Button   updateButton;
	int index;
	int eventTypeListIndex;
	int eventTeamListIndex;
	MatchController match;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		match = MatchController.getInstance();
		setContentView(R.layout.activity_edit_event);
		eventTypeList = (ListView)findViewById(R.id.listViewEventTypes);
		eventTeamList = (ListView)findViewById(R.id.listViewEventTeams);
		eventPlayerNumber = (EditText)findViewById(R.id.editTextEventPlayer);
		eventTime = (EditText)findViewById(R.id.editTextEventTime);
		updateButton = (Button)findViewById(R.id.buttonEventUpdate);
		
		eventTypeList.setChoiceMode(1);
		eventTeamList.setChoiceMode(1);
		
		eventTypeList.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				eventTypeListIndex = position;
			}});
		
		eventTeamList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				eventTeamListIndex = position;
			}});
		
		String[] types = new String[]{"GaaGoal","GaaPoint","YellowCard","RedCard","BlackCard"};
		ArrayAdapter<String> listAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_checked, types);
		eventTypeList.setAdapter(listAdapter1);
		
		String[] teams = new String[2];
		teams[0] = match.getHomeTeamName();
		teams[1] = match.getAwayTeamName();
		ArrayAdapter<String> listAdapter2 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_checked,teams);
		eventTeamList.setAdapter(listAdapter2);
		
		Bundle bundle = getIntent().getExtras();
		if (bundle != null){
			index = bundle.getInt("index");
			
			MatchEvent record = Globals.MatchReport.getRecords().get(index);
			
			eventTime.setText(record.getTimeStamp());
			eventPlayerNumber.setText(Integer.toString(record.playerNumber));
			
			int teamPosition = positionInTeam(teams, record.teamName);
			int typePosition = positionInTypes(types, record.getEventType());
			
			if (teamPosition != -1){
				eventTeamList.setItemChecked(teamPosition, true);
				eventTeamListIndex = teamPosition;
			}
			if (typePosition != -1){
				eventTypeList.setItemChecked(typePosition, true);
				eventTypeListIndex = typePosition;
			}
			
		}
		
		eventPlayerNumber.addTextChangedListener(new TextWatcher(){

			private boolean validate(String s){
				return s.matches("\\d{1,2}");
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (!validate(eventPlayerNumber.getText().toString())){
					eventPlayerNumber.setError("Player Number maximum of 2 digits");
				}
				else {
					eventPlayerNumber.setError(null);
				}
						
			}});
		eventTime.addTextChangedListener(new TextWatcher() {

			private boolean validate(String s){
				return s.matches("\\d?\\d:[0-5][0-9](\\+\\d?\\d:[0-5][0-9])?");
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (!validate(eventTime.getText().toString())){
					eventTime.setError("Enter Correct TimeStamp format XX:XX(+XX:XX)?");
				}
				else {
					eventTime.setError(null);
				}
				
			}});
		
		
	}


	private int positionInTypes(String[] eventTypes, EventTypes matchEventType) {
		for (int i = 0; i < eventTypes.length; i++){
			if (matchEventType.name().equals(eventTypes[i])){
				return i;
			}
		}
		return -1;
	}


	private int positionInTeam(String[] teams, String teamName) {
		for (int i = 0; i < teams.length; i++){
			if (teamName.equals(teams[i])){
				return i;
			}
		}
		return -1;
	}


	
	public void onUpdateEvent(View v) {
		// TODO Auto-generated method stub
		if (eventPlayerNumber.getError() != null || eventTime.getError() != null){
			Toast.makeText(this, "Error in data", Toast.LENGTH_SHORT);
		}
		else {
			Globals.MatchReport.getRecords().get(index).setEventType(MatchEvent.EventTypes.valueOf((String)eventTypeList.getItemAtPosition(eventTypeListIndex)));
			Globals.MatchReport.getRecords().get(index).playerNumber = (Integer.parseInt(eventPlayerNumber.getText().toString()));
			
			Teams teams = Teams.getInstance();
			Team team = teams.getTeamByName((String)eventTeamList.getItemAtPosition(eventTeamListIndex));
			if (team != null){
				Globals.MatchReport.getRecords().get(index).teamGuid = team.getTeamGuid();
				Globals.MatchReport.getRecords().get(index).teamId = team.getTeamID();
			}
			
			parseTimeStringIntoDurations(eventTime.getText().toString(),
					                     Globals.MatchReport.getRecords().get(index));
			
			//Set the team guid.
			//Globals.MatchReport.getRecords().get(index).setTeamName((String)eventTeamList.getItemAtPosition(eventTeamListIndex));
			//parse the string and set the timestamps.
			//Globals.MatchReport.getRecords().get(index).setTimeStamp(eventTime.getText().toString());
			
			//WCF Update.
			MatchEvent_Update updateRequest = new MatchEvent_Update();
			updateRequest.evId = Globals.MatchReport.getRecords().get(index).evId;
			updateRequest.evGuid = Globals.MatchReport.getRecords().get(index).evGuid;
			updateRequest.matchId = match.getMatchID();
			updateRequest.evType = Globals.MatchReport.getRecords().get(index).getEventType();
			updateRequest.playerId = Globals.MatchReport.getRecords().get(index).playerId;
			updateRequest.evMatchTime = Globals.MatchReport.getRecords().get(index).evMatchTime;
			updateRequest.evMatchRealTime = Globals.MatchReport.getRecords().get(index).evMatchRealTime;
			updateRequest.evOverTime = Globals.MatchReport.getRecords().get(index).evOverTime;
			updateRequest.teamGuid = Globals.MatchReport.getRecords().get(index).teamGuid;
			updateRequest.teamId = Globals.MatchReport.getRecords().get(index).teamId;
			updateRequest.playerNumber = Globals.MatchReport.getRecords().get(index).playerNumber;
			
			Thread t = new Thread(updateRequest);
			t.start();
			//Add a comment to this line
			try {
				t.join(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace()  ;
			}
			
			
		}
	}


	private void parseTimeStringIntoDurations(String string, MatchEvent matchEvent) {
		String[] components = string.split("\\+");
		DateFormat sdf = new SimpleDateFormat("mm:ss");
		if (components.length == 1){
			try {
				Date date = sdf.parse(components[0]);
				int mins = date.getMinutes();
				int secs = date.getSeconds();
				matchEvent.evMatchTimeStamp = mins*60+secs;
				matchEvent.evMatchTimeStampExtra = 0;
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (components.length == 2){
			try {
				Date date1 = sdf.parse(components[0]);
				Date date2 = sdf.parse(components[1]);
				int mins1 = date1.getMinutes();
				int secs1 = date1.getSeconds();
				int mins2 = date2.getMinutes();
				int secs2 = date2.getSeconds();
				
				matchEvent.evMatchTimeStamp = mins1*60+secs1;
				matchEvent.evMatchTimeStampExtra = mins2*60+secs2;
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}

}
