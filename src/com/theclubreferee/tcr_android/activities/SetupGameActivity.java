package com.theclubreferee.tcr_android.activities;

/**
 * @author francisbrosnan
 * This class is the activity class for match setup. This is quite a long class
 * but fairly basic. It just retrieves the values from all the data widgets and sends this to the serialisation class
 * which stores the match data
 */
import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.MatchEvent;
import com.theclubreferee.tcr_android.R;
import com.theclubreferee.tcr_android.WcfConverter;
import com.theclubreferee.tcr_android.model.GaaMatch;
import com.theclubreferee.tcr_android.model.Match;
import com.theclubreferee.tcr_android.model.MatchController;
import com.theclubreferee.tcr_android.model.Referee;
import com.theclubreferee.tcr_android.model.Referees;
import com.theclubreferee.tcr_android.model.Team;
import com.theclubreferee.tcr_android.model.Teams;
import com.theclubreferee.tcr_android.model.Venue;
import com.theclubreferee.tcr_android.model.Venues;
import com.theclubreferee.tcr_android.tweets.MatchEvent_AddNew;
import com.theclubreferee.tcr_android.tweets.MatchSetup_AddNew;
import com.theclubreferee.tcr_android.tweets.MatchSetup_Update;

public class SetupGameActivity extends Activity implements Observer{

	protected static final int REQUEST_CODE_HOMETEAM = 0;
	protected static final int REQUEST_CODE_AWAYTEAM = 1;
	final Context context = this;
	Button buttonHome;
	Button buttonAway;
	EditText selectedTeam;
	Spinner selectedTeamSpinner;
	TimePicker timep;
	DatePicker datep;
	Button buttonSaveAndAnnounce;
	EditText matchName;
	EditText matchVenue;
	EditText refName;
	Spinner  gameType;
	EditText gameDuration;
	EditText cityName;
	EditText countryName;
	MatchController controller;
	
	Resources system;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		controller = MatchController.getInstance();
		controller.addObserver(this);
		
		setContentView(R.layout.activity_setup_game);
		
		
		//This code so scroll view is set to top of view initially.
		final ScrollView mainScrollView = (ScrollView)findViewById(R.id.scrollView1);
		mainScrollView.post(new Runnable() { 
	        public void run() { 
	        	mainScrollView.fullScroll(View.FOCUS_UP);
	        } 
		});
	    
	    
	    
	    //Select Home Team
	    
	    buttonHome = (Button) findViewById(R.id.btnSaveEvent);
	    if (controller.getHomeTeamName() != null){
	    	buttonHome.setText(controller.getHomeTeamName());
	    }
	    
	    buttonHome.setOnClickListener(
	    		new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						Intent intent = new Intent(context, SelectTeamActivity.class);
						intent.putExtra("HomeOrAway", "Home");
						startActivity(intent);
					}
				  });
						
						
	    
	    //Select Away Team
	    
	    buttonAway = (Button) findViewById(R.id.button2);
	    if (controller.getAwayTeamName() != null){
	    	buttonAway.setText(controller.getAwayTeamName());
	    }
	   
	    
	    
	    buttonAway.setOnClickListener(
	    		new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						
						Intent intent = new Intent(context, SelectTeamActivity.class);
						intent.putExtra("HomeOrAway", "Away");
						startActivity(intent);;
							}
						});
						
						
	    //Assign The Date and Time Pickers.
	    timep = (TimePicker)findViewById(R.id.timePicker1);
	    //todo: classcastexception
	    //set_timepicker_text_colour();
	    datep = (DatePicker)findViewById(R.id.datePicker1);
	    
	  //Text Boxes
	    matchName  = (EditText)findViewById(R.id.editText1);
	    
	    
	    matchVenue = (EditText)findViewById(R.id.editText2);
	    refName    = (EditText)findViewById(R.id.editText3);
	    gameType   = (Spinner)findViewById(R.id.spinner1);
	    gameDuration = (EditText)findViewById(R.id.editTextPlayDuration);
	    cityName   = (EditText)findViewById(R.id.editText5);
	    countryName= (EditText)findViewById(R.id.editText6);
	    
	    /*
	     * match was previously announced load up saved settings.
	     */
	    if (controller.isSetUp())
	    {
	    	timep.setCurrentHour(controller.getKickOffDate().get(Calendar.HOUR_OF_DAY));
	    	timep.setCurrentMinute(controller.getKickOffDate().get(Calendar.MINUTE));
	    	datep.updateDate(controller.getKickOffDate().get(Calendar.YEAR), 
	    			         controller.getKickOffDate().get(Calendar.MONTH), 
	    			         controller.getKickOffDate().get(Calendar.DAY_OF_MONTH));
	    
	    	matchName.setText(controller.getGameTitle());
	    	//gameType.setText(currentSettings.getSportTypeID().toString());
	    	if (controller.isVenueSetup()){
	    		Venue lastVenue = Venues.getInstance().getVenueByName(controller.getVenueName());
	    		if (lastVenue != null)
	    		{
	    			matchVenue.setText(lastVenue.getVenueName());
	    			cityName.setText(lastVenue.getCityPlaceID());
	    			countryName.setText(lastVenue.getCountry());
	    		}
	    	}
	    	
	    	Referee lastRef = findRefereeByGuid(Referees.getInstance(),controller.getRefereeGuid());
	    	if (lastRef != null)
	    	{
	    		refName.setText(lastRef.getName());
	    	}
	    	gameDuration.setText(Integer.toString(controller.getMatchDuration()));
	    }
	    else 
	    {
	    	//set default referee name.
	    	if (Referees.getInstance().getDefaultReferee() != null){
	    		refName.setText(Referees.getInstance().getDefaultReferee().getName());
	    	}
	    	//set default for gaa match
	    	gameDuration.setText("70");
	    }
	    
	        
	    // Look up cityName and country name if venue is known.
	    matchVenue.setOnFocusChangeListener(new OnFocusChangeListener() {          

	        public void onFocusChange(View v, boolean hasFocus) {
	            if (!hasFocus){
	            	Venue myVenue = findVenueByName(Venues.getInstance(),matchVenue.getText().toString());
	            	if (myVenue != null)
	            	{
	            		cityName.setText(myVenue.getCityPlaceID());
	            		countryName.setText(myVenue.getCountry());
	            	}
	            } 
	        }
	    });
	    
	    
	    //Save button logic
	    buttonSaveAndAnnounce = (Button)findViewById(R.id.button3);
	    
	    buttonSaveAndAnnounce.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// Create a new MatchSettings object or edit existing one.
				// Validate Fields
				// Extract all input values
				// Update MatchSettings.
				Calendar kickOffDetails = new GregorianCalendar();
				kickOffDetails.set(datep.getYear(), datep.getMonth(), datep.getDayOfMonth(), 
						     timep.getCurrentHour(), timep.getCurrentMinute());
				Match.SportTypes gameTypeValue = convertGameTypeFromString(String.valueOf(gameType.getSelectedItem()));
				
				//validate fields. for text
				if (buttonHome.getText().toString().equals("Select Team") ||
					buttonAway.getText().toString().equals("Select Team") ||
					matchName.getText().toString().isEmpty()           	  ||
					refName.getText().toString().isEmpty()                ||
					matchVenue.getText().toString().isEmpty()             ||
					cityName.getText().toString().isEmpty()               ||
					gameDuration.getText().toString().isEmpty()           ||
					countryName.getText().toString().isEmpty()){
					Toast.makeText(getApplicationContext(), "Please Enter All Required Fields", Toast.LENGTH_SHORT).show();
					return;
				}
				
				
				//populate the currentSettings.
				if (!controller.isSetUp()){
					controller.setMatchGuid(UUID.randomUUID());
				}
				
				
				controller.setAwayTeam(buttonAway.getText().toString());
				controller.getAwayTeam().resetTeamScore();
				controller.setGameTitle(matchName.getText().toString());
				controller.setSetUp(true);
				controller.setHomeTeam(buttonHome.getText().toString());
				controller.getHomeTeam().resetTeamScore();
				controller.setKickOffDate(kickOffDetails);
				
				
				
				//Create new referee
				String refsName=refName.getText().toString();
				if (!refsName.contains(" "))
				{
					Toast.makeText(getApplicationContext(), "Please Enter First Name and Surname for referee", Toast.LENGTH_SHORT).show();
					return;
				}
				String firstName = refsName.substring(0, refsName.indexOf(' '));
				String surName = refsName.substring(refsName.indexOf(' ')+1);
				Referee referee = Referees.getInstance().getRefereeByName(firstName, surName);
				
				if (referee==null)
				{
					 referee = new Referee(firstName, surName, firstName+surName, false);
				}
				controller.setReferee(referee.getFirstName(), referee.getLastName());
				controller.setSportTypeID(gameTypeValue);
				controller.setMatchDuration(Integer.parseInt(gameDuration.getText().toString()));
				
				//Setup venue
				String venueName=matchVenue.getText().toString();
				Venue venue = Venues.getInstance().getVenueByName(venueName);
				
				if (venue==null)
				{
					 venue = new Venue(venueName,cityName.getText().toString(),countryName.getText().toString());
				}
				controller.setVenue(venue.getVenueName());
				
				controller.setMatchPeriod(Match.MatchPeriod.MatchNotStarted);
				controller.save();	
				
				
				
				
//				mySerialization.saveMatchSetup(buttonHome.getText().toString(), 
//						                       buttonAway.getText().toString(), 
//						                       matchName.getText().toString(), 
//						                       kickOffDetails, 
//						                       refName.getText().toString(), 
//						                       //gameType.getText().toString(), 
//						                       Match.SportTypes.GaaFootball,  //Ray: Hard coded.
//						                       matchVenue.getText().toString(), 
//						                       cityName.getText().toString(), 
//						                       countryName.getText().toString());
				
					Toast.makeText(getApplicationContext(), "Match Setup Succesfully", Toast.LENGTH_SHORT).show();
					
					if (controller.getMatchID() == 0)
					{
						//Now save match to system.
						MatchSetup_AddNew newMatchSetUpRequest = new MatchSetup_AddNew();
						newMatchSetUpRequest.matchGUID         = controller.getMatchGuid();
							newMatchSetUpRequest.homeTeamId        = controller.getHomeTeamID();
							newMatchSetUpRequest.awayTeamId        = controller.getAwayTeamID();
						//TODO : Hard Coded.
						newMatchSetUpRequest.venueID           = 10000;
						
						newMatchSetUpRequest.matchTitle        = WcfConverter.convertToWcfFormat(matchName.getText().toString());
						newMatchSetUpRequest.scheduledKickOff  = kickOffDetails.getTime();
						newMatchSetUpRequest.actualKickOff     = kickOffDetails.getTime();
							newMatchSetUpRequest.userID            = controller.getRefereeID();
							newMatchSetUpRequest.creatorID         = controller.getRefereeID();
						newMatchSetUpRequest.sportID           = gameTypeValue.value;
						//TODO: Hard Coded
						newMatchSetUpRequest.password          = "mypass";
						newMatchSetUpRequest.playDuration     = 40;
						newMatchSetUpRequest.numberOfPlays     = 2;
					
						//populate the request
						new Thread(newMatchSetUpRequest).start();
						synchronized (newMatchSetUpRequest){
							try {
								newMatchSetUpRequest.wait(1000);
							
								if (!Globals.isNumeric(Globals.NewMatchResponse)){
									Toast.makeText(getApplicationContext(), "Match Setup Not Saved to server", Toast.LENGTH_SHORT).show();
								}
								else {
									controller.setMatchID(Integer.parseInt(Globals.NewMatchResponse));
								}
							
							controller.save();
							
							//Globals.AllMatchEvents.triggerObservers();
							
							//TODO Another call to serialize inefficient
							//TODO Errors
							//mySerialization.saveReferees(currentReferees);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					else { //match has a valid id so is an update.
						//Now save match to system.
						MatchSetup_Update updateMatchSetUpRequest = new MatchSetup_Update();
						updateMatchSetUpRequest.matchId        = controller.getMatchID();
						updateMatchSetUpRequest.matchGUID         = controller.getMatchGuid();
							updateMatchSetUpRequest.homeTeamId        = controller.getHomeTeamID();
							updateMatchSetUpRequest.awayTeamId        = controller.getAwayTeamID();
						//TODO : Hard Coded.
						updateMatchSetUpRequest.venueID           = 10000;
						updateMatchSetUpRequest.matchTitle        = WcfConverter.convertToWcfFormat(matchName.getText().toString());
						updateMatchSetUpRequest.scheduledKickOff  = kickOffDetails.getTime();
						updateMatchSetUpRequest.actualKickOff     = kickOffDetails.getTime();
						
							updateMatchSetUpRequest.userID            = controller.getRefereeID();
							updateMatchSetUpRequest.creatorID         = controller.getRefereeID();
						updateMatchSetUpRequest.sportID           = gameTypeValue.value;
						//TODO: Hard Coded
						updateMatchSetUpRequest.password          = "mypass";
						updateMatchSetUpRequest.playDuration     = 40;
						updateMatchSetUpRequest.numberOfPlays     = 2;
					
						//populate the request
						new Thread(updateMatchSetUpRequest).start();
						synchronized (updateMatchSetUpRequest){
							try {
								updateMatchSetUpRequest.wait(1000);
							
								//TODO: How do we know the request is successful.
							
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					
					announceMatch(controller.getMatchID(),kickOffDetails.getTime());
					setResult(RESULT_OK, getIntent());
					finish();
					//Globals.AllMatchEvents.triggerObservers();
					
				
			}

			private void announceMatch(int matchId, Date date) {
				MatchEvent_AddNew announceEvent = new MatchEvent_AddNew();
				announceEvent.evType = MatchEvent.EventTypes.GaaMatchAnnounced;
				announceEvent.evGuid = UUID.randomUUID();
				announceEvent.evMatchRealTime = new Date();
				announceEvent.evMatchTime = date;
				announceEvent.evOverTime = new Date();
				announceEvent.matchId = matchId;
				
				new Thread(announceEvent).start();
				synchronized (announceEvent){
					try {
						announceEvent.wait(1000);
					
						//TODO: How do we know the request is successful.
					
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}

			private Referee findIfRefereeExists(String name,
					Referees currentReferees) {
				for (Referee referee : currentReferees.getAllReferees())
				{
					if (referee.getName().equalsIgnoreCase(name))
					{
						return referee;
					}
				}
				return null;
			}

			private Venue findIfVenueExists(String venueName, Venues currentVenues) {
				// TODO Auto-generated method stub
				for (Venue venue : currentVenues.getAllVenues())
				{
					if (venue.getVenueName().equalsIgnoreCase(venueName))
					{
						return venue;
					}
				}
				return null;
			}

			private Team findIfTeamExists(String teamName, Teams currentTeams) {
				// TODO Auto-generated method stub
				for (Team team : currentTeams.getAllTeams())
				{
					if (team.getTeamName().equalsIgnoreCase(teamName))
						return team;
				}
				return null;
			}
	    	
	    });
	    
	    
	}

	private Referee findRefereeByGuid(Referees currentReferees,
			UUID refereeGuid) {
		for (Referee ref : currentReferees.getAllReferees())
		{
			if (ref.getRefereeGuid().equals(refereeGuid))
			{
				return ref;
			}
		}
		return null;
	}

	

	

			private Referee findIfRefereeExists(String name,
					Referees currentReferees) {
				for (Referee referee : currentReferees.getAllReferees()) {
					if (referee.getName().equalsIgnoreCase(name)) {
							return referee;
						}
				}
				return null;
			}

			private Venue findIfVenueExists(String venueName,
					Venues currentVenues) {
				// TODO Auto-generated method stub
				for (Venue venue : currentVenues.getAllVenues()) {
					if (venue.getVenueName().equalsIgnoreCase(venueName)) {
						return venue;
					}
				}
				return null;
			}

			private Team findIfTeamExists(String teamName, Teams currentTeams) {
				// TODO Auto-generated method stub
				for (Team team : currentTeams.getAllTeams()) {
					if (team.getTeamName().equalsIgnoreCase(teamName))
						return team;
				}
				return null;
			}



	

	private Venue findVenueByGUIId(UUID venueId, Venues currentVenues) {
		// TODO Auto-generated method stub
		for (Venue venue : currentVenues.getAllVenues()) {
			if (venue.getVenueGuid().equals(venueId)) {
				return venue;
			}
		}
		return null;
	}

	private Team findTeamByGuid(Teams currentTeams, UUID aTeamGuid) {
		for (Team team : currentTeams.getAllTeams()) {
			if (team.getTeamGuid().equals(aTeamGuid)) {
				return team;
			}
		}
		return null;
	}

	private Venue findVenueByName(Venues currentVenues, String venueName) {
		for (Venue venue : currentVenues.getAllVenues()) {
			if (venue.getVenueName().equalsIgnoreCase(venueName)) {
				return venue;
			}
		}
		return null;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_setup_game, menu);
		return true;
	}
	
	
//	 <item>Hurling</item>
//     <item>Camogie</item>
//     <item>Rugby Union</item>
//     <item>Rugby League</item>
//     <item>Tag Rugby</item>
//     <item>Soccer</item>
//     <item>American Football</item>
	
	protected Match.SportTypes convertGameTypeFromString(String str){
		Match.SportTypes gameType = Match.SportTypes.GaaFootball;
		if (str.equals("GaaFootball")){
			gameType = Match.SportTypes.GaaFootball;
		}
		else if (str.equals("Hurling")){
			gameType = Match.SportTypes.Hurling;
		}
		else if (str.equals("Camogie")){
			gameType = Match.SportTypes.Camogie;
		}
		else if (str.equals("Rugby Union")){
			gameType = Match.SportTypes.Union;
		}
		else if (str.equals("Rugby League")){
			gameType = Match.SportTypes.League;
		}
		else if (str.equals("Tag Rugby")){
			gameType = Match.SportTypes.Tag;
		}
		else if (str.equals("Soccer")){
			gameType = Match.SportTypes.Soccer;
		}
		else if (str.equals("American Football")){
			gameType = Match.SportTypes.Football;
		}
		return gameType;
	}
	
	//Note : this has to be done to get the right color for the timepicker
		//Android sucks :-).
		
		private void set_timepicker_text_colour(){
		    system = Resources.getSystem();
		    int hour_numberpicker_id = system.getIdentifier("hour", "id", "android");
		    int minute_numberpicker_id = system.getIdentifier("minute", "id", "android");
		    int ampm_numberpicker_id = system.getIdentifier("amPm", "id", "android");

		    NumberPicker hour_numberpicker = (NumberPicker) timep.findViewById(hour_numberpicker_id);
		    NumberPicker minute_numberpicker = (NumberPicker) timep.findViewById(minute_numberpicker_id);
		    NumberPicker ampm_numberpicker = (NumberPicker) timep.findViewById(ampm_numberpicker_id);

		    set_numberpicker_text_colour(hour_numberpicker);
		    set_numberpicker_text_colour(minute_numberpicker);
		    set_numberpicker_text_colour(ampm_numberpicker);
		}
		
		private void set_numberpicker_text_colour(NumberPicker number_picker){
		    final int count = number_picker.getChildCount();
		    final int color = getResources().getColor(R.color.White);

		    for(int i = 0; i < count; i++){
		        View child = number_picker.getChildAt(i);

		        try{
		            Field wheelpaint_field = number_picker.getClass().getDeclaredField("mSelectorWheelPaint");
		            wheelpaint_field.setAccessible(true);

		            ((Paint)wheelpaint_field.get(number_picker)).setColor(color);
		            ((EditText)child).setTextColor(color);
		            number_picker.invalidate();
		        }
		        catch(NoSuchFieldException e){
		            Log.w("setNumberPickerTextColor", e);
		        }
		        catch(IllegalAccessException e){
		            Log.w("setNumberPickerTextColor", e);
		        }
		        catch(IllegalArgumentException e){
		            Log.w("setNumberPickerTextColor", e);
		        }
		    }
		}

		@Override
		public void update(Observable arg0, Object arg1) {
			// TODO Auto-generated method stub
			if (controller.getHomeTeamName()!=null)
			{
				buttonHome.setText(controller.getHomeTeamName());
			}
			if (controller.getAwayTeamName()!=null)
			{
				buttonAway.setText(controller.getAwayTeamName());
			}
			
		}

}


