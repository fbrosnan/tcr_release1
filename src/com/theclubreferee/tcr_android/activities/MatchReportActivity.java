package com.theclubreferee.tcr_android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.MatchEvent;
import com.theclubreferee.tcr_android.R;
import com.theclubreferee.tcr_android.model.Match;
import com.theclubreferee.tcr_android.tweets.MatchEvent_Delete;

public class MatchReportActivity extends Activity {

	ListView editReport;
	ArrayAdapter<MatchEvent> adapter;
	int _listPosition;
	Match match;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		match=(Match)(this.getIntent().getSerializableExtra("match"));
		setContentView(R.layout.activity_match_report);
		
		//will cause froyo to crash.
		// Show the Up button in the action bar.
		//getActionBar().setDisplayHomeAsUpEnabled(true);
		editReport = (ListView)findViewById(R.id.listViewMatchReport);
		//editReport.setText(Globals.MatchReport.toString());
		adapter = new ArrayAdapter<MatchEvent>(this,android.R.layout.simple_list_item_1,Globals.MatchReport.getRecords());
		editReport.setAdapter(adapter);
		
		registerForContextMenu(editReport);
		
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		//catches changes from updates.
		adapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_match_report, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override 
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
	    AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
	    _listPosition = info.position;      // Get Index of long-clicked item

	    super.onCreateContextMenu(menu, v, menuInfo);
	    menu.setHeaderTitle("Event Action");   // Context-menu title
	    menu.add(0, v.getId(), 0, "Edit");  // Add element "Edit"
	    menu.add(0, v.getId(), 1, "Delete");        // Add element "Delete"
	}
	
	@Override  
	 public boolean onContextItemSelected(MenuItem item)
	 {
	      if(item.getTitle() == "Edit") // "Edit" chosen
	      {
	         // Activity Edit Screen
	    	 Intent intent = new Intent(this, MatchEventsEditActivity.class);
	    	 intent.putExtra("index",_listPosition);
	    	 intent.putExtra("match", match);
			 startActivity(intent);
	    	 
	      }
	      else if(item.getTitle() == "Delete")  // "Delete" chosen
	      {
	          // Delete Model
	    	  int evId = Globals.MatchReport.getRecords().get(_listPosition).evId;
	    	  Globals.MatchReport.getRecords().remove(_listPosition);
	    	  adapter.notifyDataSetChanged();
	    	  // WCF Delete Request
	    	  MatchEvent_Delete deleteRequest = new MatchEvent_Delete();
	    	  deleteRequest.evId = evId;
	    	  Thread t = new Thread(deleteRequest);
			  t.start();
			  //Add a comment to this line
			  try {
				t.join(1000);
			  } catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace()  ;
			  }
	    	  
	      }
	      else 
	      {
	         return false;
	      }

	      return true;  
	 }  

}
