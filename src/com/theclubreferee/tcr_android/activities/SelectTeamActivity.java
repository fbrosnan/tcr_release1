package com.theclubreferee.tcr_android.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.theclubreferee.tcr_android.R;
import com.theclubreferee.tcr_android.model.MatchController;
import com.theclubreferee.tcr_android.model.Team;
import com.theclubreferee.tcr_android.model.Teams;

public class SelectTeamActivity extends ListActivity implements OnClickListener {

	Button addButton;
	ListView teamList;
	private ArrayAdapter<Team> itemAdapter;
	boolean  homeFlag;
	private MatchController controller;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		controller=MatchController.getInstance();
		setContentView(R.layout.activity_select_team);
		addButton    = (Button)findViewById(R.id.buttonAddTeam);
		teamList     = (ListView)findViewById(android.R.id.list);
		
		//populate the list view table.
		
		Teams currentTeams = Teams.getInstance();
		
		itemAdapter = new ArrayAdapter<Team>(this,
		                                                    android.R.layout.simple_list_item_1, 
		                                                    currentTeams.getAllTeams());
		setListAdapter(itemAdapter);
		
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		Bundle extras = getIntent().getExtras();
		if (extras == null) {
		  return;
		}
		// get data via the key
		String value1 = extras.getString("HomeOrAway");
		if (value1 != null) {
		  if (value1.equalsIgnoreCase("Home")){
			  homeFlag = true;
		  }
		  else if (value1.equalsIgnoreCase("Away")) {
			  homeFlag = false;
		  }
		}
		
	}
	
	@Override
	public void onResume(){
		super.onResume();
		Teams currentTeams = Teams.getInstance();
		
		itemAdapter = new ArrayAdapter<Team>(this,
                android.R.layout.simple_list_item_1, 
                currentTeams.getAllTeams());
		setListAdapter(itemAdapter);
		itemAdapter.notifyDataSetChanged();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_select_team, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == addButton){
			Intent intent = new Intent(this, EditTeamActivity.class);
			startActivity(intent);
		}
	}
	
	 @Override
	  protected void onListItemClick(ListView l, View v, int position, long id) {
	    Team item = (Team) getListAdapter().getItem(position);
	    if (homeFlag)
	    	controller.setHomeTeam(item.getTeamName());
	    else
	    	controller.setAwayTeam(item.getTeamName());
	    
	    setResult(RESULT_OK, getIntent());
	    finish();
	  }

}
