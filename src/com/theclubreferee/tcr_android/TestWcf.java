package com.theclubreferee.tcr_android;

import com.theclubreferee.tcr_android.activities.TestWcfActivity;

import android.content.Context;
import android.content.Intent;

public class TestWcf {
	private final Context context;
	  
	public TestWcf(Context c){
		
		this.context = c;
	}
	
	public void ShowTestWcfActivity()
	{
		Intent intent = new Intent(context, TestWcfActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}
}
