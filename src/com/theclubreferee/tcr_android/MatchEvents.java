/*Original code by Ray 12 May 2014
 * A collection class containing all events of type MatchEvent.java
 * Insert functions here for getting scores / yellow card count per team etc
 */

package com.theclubreferee.tcr_android;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.UUID;

import com.theclubreferee.tcr_android.model.MatchController;

public class MatchEvents extends Observable implements Serializable {

	static final long serialVersionUID = -1371901740781837358L;

	private MatchController match;
	  
	public MatchEvents(){
		
		this.match=MatchController.getInstance();
	}

	private ArrayList<MatchEvent> matchEvents = new ArrayList<MatchEvent>();

	public ArrayList<MatchEvent> getAllMatchEvents() {
 		return matchEvents;
 	}

	public void setMatchEvents(ArrayList<MatchEvent> allVenues) {
		this.matchEvents = allVenues;
	}

	// Send any pending MatchEvent to the server
	public void TransmitEvents() {
		try{
			for (MatchEvent me : matchEvents) {
			if (me.UploadSuccess == false)
				me.SendEvent();
			}	
			}
			finally{
				triggerObservers();
			}
	}

	public int getHomeGaaGoals() {
		int i = 0;

		for (MatchEvent ev : matchEvents)
			if (ev.getEventType() == MatchEvent.EventTypes.GaaGoal & ev.teamGuid == match.getHomeTeamGuid())
 				i++;

		return i;
	}

	public int getHomeGaaPoints() {
		int i = 0;

		for (MatchEvent ev : matchEvents)
			if (ev.getEventType() == MatchEvent.EventTypes.GaaPoint & ev.teamGuid == match.getHomeTeamGuid())
				i++;

		return i;
	}

	
	public int getAwayGaaGoals() {
		int i = 0;

		for (MatchEvent ev : matchEvents)
			if (ev.getEventType() == MatchEvent.EventTypes.GaaGoal & ev.teamGuid == match.getAwayTeamGuid())
 				i++;

		return i;
	}

	public int getAwayGaaPoints() {
		int i = 0;

		for (MatchEvent ev : matchEvents)
			if (ev.getEventType() == MatchEvent.EventTypes.GaaPoint & ev.teamGuid == match.getAwayTeamGuid())
				i++;

		return i;
	}
	
	//Updates after it has been uploaded to the server
	public void updateEventId(UUID evGuid, int eventId){
		for(MatchEvent ev: matchEvents){
			if(ev.evGuid == evGuid){
				ev.evId= eventId;
				ev.UploadSuccess = true;
			}		
		}		
	}

	public void add(MatchEvent ev) {
		matchEvents.add(ev);
		triggerObservers();
	}

	// Create a method to update the Observerable's flag to true for changes and
	// notify the observers to check for a change. These are also a part of the
	// secret sauce that makes Observers and Observables communicate
	// predictably.
	public void triggerObservers() {

		setChanged();
		notifyObservers();
	}

	public MatchEvent createNewEvent() {
		// TODO Auto-generated method stub
		return new MatchEvent();
	}


}
