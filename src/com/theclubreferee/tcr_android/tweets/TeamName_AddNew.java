//This class is for communication with the Web Services
//for sending data to the web

package com.theclubreferee.tcr_android.tweets;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;


import com.theclubreferee.tcr_android.Globals;



//@Frank this will obviously need values from the New Team form & button.
//WCF will not save the teamName if it already exists in the database.
	
	//How to call the function 

	//SaveTeamName stn = new SaveTeamName();
	//stn.TeamName = "Tallaght RFC";
	//stn.Description = "West Dublin";
	//stn.TeamGuid = UUID.randomUUID();
	//new Thread(stn).start();
	
public class TeamName_AddNew implements Runnable {
//can't see commits111
			
	public String TeamName;
	public String Description;
	public UUID TeamGuid;  //UUID.fromString("00000000-0000-0000-0000-000000000001");
	
	@Override
	public void run() {
		try {

			synchronized (this) {
				DefaultHttpClient client = new DefaultHttpClient();
				String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/TeamName_AddNew/";
				EMPLOYEE_SERVICE_URI += "?teamName=" + TeamName;
				EMPLOYEE_SERVICE_URI += "&teamDescription=" + Description;
				EMPLOYEE_SERVICE_URI += "&teamGuid=" + TeamGuid.toString();
						
			
				//String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/SaveTeamName/";
				HttpGet request = new HttpGet(EMPLOYEE_SERVICE_URI);

				// set the header to get the data in JSON format
				request.setHeader("Accept", "application/json");
				request.setHeader("Content-type", "application/json");

				// get the response
				HttpResponse response = client.execute(request);

				HttpEntity entity = response.getEntity();

				// if entity content lenght 0, means no employee exist in the system
				// with these code
				if (entity.getContentLength() != 0) {
					// stream reader object
					Reader employeeReader = new InputStreamReader(response.getEntity().getContent());
					// create a buffer to fill if from reader
					char[] buffer = new char[(int) response.getEntity().getContentLength()];
					// fill the buffer by the help of reader
					employeeReader.read(buffer);
					// close the reader streams
					employeeReader.close();

					String s = new String(buffer).replaceAll("\"","");
					
					// for the employee json object
					// String s = new String(buffer);

					Globals.TeamNameResponse = new String(s);
				}
				this.notifyAll();
			}

		} catch (Exception e) {
			// any exception show the error layout

			Globals.TeamNameResponse += e.getMessage() + e.getLocalizedMessage();
			

		}
	}

}
