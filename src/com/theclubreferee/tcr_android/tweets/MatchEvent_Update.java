//This class is for communication with the Web Services
//for sending data to the web

package com.theclubreferee.tcr_android.tweets;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Date;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.MatchEvent.EventTypes;


	
public class MatchEvent_Update implements Runnable {

	public int evId = 10004;
	public UUID evGuid;
	public int matchId;
	public EventTypes evType;
	public UUID playerId;
	public Date evMatchTime;
	public Date evMatchRealTime; 	//don't know if this is needed.
	public Date evOverTime;
	public UUID teamGuid;			//used on device when no internet found
	public int	teamId;				//used after internet trip is made
	public int playerNumber;			
	
	@Override
	public void run() {
		try {

			DefaultHttpClient client = new DefaultHttpClient();
			String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/MatchEvent_Update/";
			EMPLOYEE_SERVICE_URI += "?eventId=" + evId;
			EMPLOYEE_SERVICE_URI += "&eventGuid=" + evGuid;
			EMPLOYEE_SERVICE_URI += "&matchId=" + matchId;
			EMPLOYEE_SERVICE_URI += "&eventTypeId=" + 1000;
			EMPLOYEE_SERVICE_URI += "&playerGuid=" + "00000000-0000-0000-0000-000000000000";
			EMPLOYEE_SERVICE_URI += "&eventMatchTime=" + Globals.FormatDateWCF(evMatchTime);
			EMPLOYEE_SERVICE_URI += "&eventRealTime=" + Globals.FormatDateWCF(evMatchRealTime);
			EMPLOYEE_SERVICE_URI += "&eventOvertime=" + Globals.FormatDateWCF(evOverTime);
			EMPLOYEE_SERVICE_URI += "&teamId=" + teamId;
			EMPLOYEE_SERVICE_URI += "&playerNumber=" + playerNumber;
				
			

			
			//String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/SaveTeamName/";
			HttpGet request = new HttpGet(EMPLOYEE_SERVICE_URI);

			// set the header to get the data in JSON format
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");

			// get the response
			HttpResponse response = client.execute(request);

			HttpEntity entity = response.getEntity();

			// if entity content lenght 0, means no employee exist in the system
			// with these code
			if (entity.getContentLength() != 0) {
				// stream reader object
				Reader employeeReader = new InputStreamReader(response.getEntity().getContent());
				// create a buffer to fill if from reader
				char[] buffer = new char[(int) response.getEntity().getContentLength()];
				// fill the buffer by the help of reader
				employeeReader.read(buffer);
				// close the reader streams
				employeeReader.close();

				// for the employee json object
				// String s = new String(buffer);

				Globals.MatchEventResponse = new String(buffer);
			}

		} catch (Exception e) {
			// any exception show the error layout

			Globals.MatchEventResponse += e.getMessage() + e.getLocalizedMessage();
			

		}
	}

}
