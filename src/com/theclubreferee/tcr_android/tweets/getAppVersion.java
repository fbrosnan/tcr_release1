//This class will get the lastest AppVersion from the Server.

package com.theclubreferee.tcr_android.tweets;

import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.theclubreferee.tcr_android.Globals;

//This class is for communication with the Web Services
//for sending data to the web

public class getAppVersion implements Runnable {

	@Override
	public void run() {
		try {

			DefaultHttpClient client = new DefaultHttpClient();
			String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/AppVersion/";
			HttpGet request = new HttpGet(EMPLOYEE_SERVICE_URI);

			// set the header to get the data in JSON format
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");

			// get the response
			HttpResponse response = client.execute(request);

			HttpEntity entity = response.getEntity();

			// if entity content lenght 0, means no employee exist in the system
			// with these code
			if (entity.getContentLength() != 0) {
				// stream reader object
				Reader employeeReader = new InputStreamReader(response.getEntity().getContent());
				// create a buffer to fill if from reader
				char[] buffer = new char[(int) response.getEntity().getContentLength()];
				// fill the buffer by the help of reader
				employeeReader.read(buffer);
				// close the reader streams
				employeeReader.close();

				// for the employee json object
				// String s = new String(buffer);

				Globals.AppVersion = new JSONObject(new String(buffer));
			}

		} catch (Exception e) {
			// any exception show the error layout

			// return "Error1: " + e.getMessage() + e.getLocalizedMessage();

		}
	}

}
