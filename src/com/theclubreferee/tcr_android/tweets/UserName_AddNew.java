//This class is for communication with the Web Services
//for sending data to the web

package com.theclubreferee.tcr_android.tweets;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import com.theclubreferee.tcr_android.Globals;


	
public class UserName_AddNew implements Runnable {

	public int UserId; //<<<<<<< REQUIRED
	public String FirstName;
	public String LastName;
	public String UserName;
	public String AppVersion;
	public UUID UserGuid;
	public UUID AppGuid;
	
	@Override
	public void run() {
		try {

			synchronized (this) {
				DefaultHttpClient client = new DefaultHttpClient();
				String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/UserName_AddNew/";
				EMPLOYEE_SERVICE_URI += "?userFirstName=" + FirstName;
				EMPLOYEE_SERVICE_URI += "&userLastName=" + LastName;
				EMPLOYEE_SERVICE_URI += "&userUserName=" + UserName;
				EMPLOYEE_SERVICE_URI += "&userAppVersion=" + AppVersion;
				EMPLOYEE_SERVICE_URI += "&userGuid=" + UserGuid.toString();
				EMPLOYEE_SERVICE_URI += "&appGuid=" + AppGuid.toString();
						
			
				//String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/SaveTeamName/";
				HttpGet request = new HttpGet(EMPLOYEE_SERVICE_URI);

				// set the header to get the data in JSON format
				request.setHeader("Accept", "application/json");
				request.setHeader("Content-type", "application/json");

				// get the response
				HttpResponse response = client.execute(request);

				HttpEntity entity = response.getEntity();

				// if entity content lenght 0, means no employee exist in the system
				// with these code
				if (entity.getContentLength() != 0) {
					// stream reader object
					Reader employeeReader = new InputStreamReader(response.getEntity().getContent());
					// create a buffer to fill if from reader
					char[] buffer = new char[(int) response.getEntity().getContentLength()];
					// fill the buffer by the help of reader
					employeeReader.read(buffer);
					// close the reader streams
					employeeReader.close();

					// for the employee json object
					String s = new String(buffer).replaceAll("\"","");

					Globals.UserNewNameResponse = new String(s);
				}
				this.notifyAll();
			}

		} catch (Exception e) {
			// any exception show the error layout

			Globals.TeamNameResponse += e.getMessage() + e.getLocalizedMessage();
			

		}
	}

}
