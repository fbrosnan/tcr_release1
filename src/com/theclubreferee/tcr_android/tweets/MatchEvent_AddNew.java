//This class is for communication with the Web Services
//for sending data to the web

package com.theclubreferee.tcr_android.tweets;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Date;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.MatchEvent.EventTypes;
import com.theclubreferee.tcr_android.MatchEvent;
import java.util.Observable;

	
public class MatchEvent_AddNew extends Observable implements Runnable {

	public UUID evGuid;
	public int matchId;
	public EventTypes evType;
	public UUID playerId;
	public Date evMatchTime;
	public Date evMatchRealTime; 	//don't know if this is needed.
	public Date evOverTime;
	public UUID teamGuid;			//used on device when no internet found
	public int	teamId = 0;				//used after internet trip is made
	public int playerNumber =0;			
	
	@Override
	public void run() {
		try {

			DefaultHttpClient client = new DefaultHttpClient();
			String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/MatchEvent_AddNew/";
			EMPLOYEE_SERVICE_URI += "?eventGuid=" + evGuid;
			EMPLOYEE_SERVICE_URI += "&matchId=" + matchId;
			EMPLOYEE_SERVICE_URI += "&eventTypeId=" + evType.getValue();
			EMPLOYEE_SERVICE_URI += "&playerGuid=" + "00000000-0000-0000-0000-000000000000";
			EMPLOYEE_SERVICE_URI += "&eventMatchTime=" + Globals.FormatDateWCF(evMatchTime);
			EMPLOYEE_SERVICE_URI += "&eventRealTime=" + Globals.FormatDateWCF(evMatchRealTime);
			EMPLOYEE_SERVICE_URI += "&eventOvertime=" + Globals.FormatDateWCF(evOverTime);
			EMPLOYEE_SERVICE_URI += "&teamId=" + teamId;
			EMPLOYEE_SERVICE_URI += "&playerNumber=" + playerNumber;
				
			

			
			//String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/SaveTeamName/";
			HttpGet request = new HttpGet(EMPLOYEE_SERVICE_URI);

			// set the header to get the data in JSON format
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");

			// get the response
			HttpResponse response = client.execute(request);

			HttpEntity entity = response.getEntity();

			// if entity content lenght 0, means no employee exist in the system
			// with these code
			if (entity.getContentLength() != 0) {
				// stream reader object
				Reader employeeReader = new InputStreamReader(response.getEntity().getContent());
				// create a buffer to fill if from reader
				char[] buffer = new char[(int) response.getEntity().getContentLength()];
				// fill the buffer by the help of reader
				employeeReader.read(buffer);
				// close the reader streams
				employeeReader.close();

				// for the employee json object
				// String s = new String(buffer);

				String s = new String(buffer).replaceAll("\"","");
				
				int rtnId = Integer.parseInt(s);
				Globals.MatchEventResponse = (new String(buffer));
								
				int evId = Integer.parseInt(s);
								
								
				//WHY WILL THIS NOT WORK?  WHY CAN I NOT DEBUG IT?
				for(MatchEvent ev: Globals.AllMatchEvents.getAllMatchEvents()){
					if(ev.evGuid == evGuid){
						ev.evId= rtnId;
						ev.UploadSuccess = true;
					}
				}
								
			    //Globals.AllMatchEvents.updateEventId(this.evGuid, evId);
												
								
								
			}

		} catch (Exception e) {
			// any exception show the error layout

			Globals.MatchEventResponse += e.getMessage() + e.getLocalizedMessage();
			

		}
		finally{
			triggerObservers();
		}
	}
	
	// Create a method to update the Observerable's flag to true for changes and
	// notify the observers to check for a change. These are also a part of the
	// secret sauce that makes Observers and Observables communicate
	// predictably.
	private void triggerObservers() {
		setChanged();
		notifyObservers();
	}

}
