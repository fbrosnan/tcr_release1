package com.theclubreferee.tcr_android.tweets;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Date;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.theclubreferee.tcr_android.Globals;

public class MatchSetup_AddNew implements Runnable {
	
	public UUID 	matchGUID;
	public int  	homeTeamId;
	public int  	awayTeamId;
	public int  	venueID;
	public String 	matchTitle;
	public Date     scheduledKickOff;
	public Date     actualKickOff;
	public int      userID;
	public int      creatorID;
	public int      sportID;
	public String   password;
	public int      playDuration;
	public int      numberOfPlays;
	
	@Override
	public void run() {
		try {
			synchronized (this) {
		DefaultHttpClient client = new DefaultHttpClient();
		//Service at http://www.theclubreferee.com/EmployeeInfo.svc/Match_AddNew/?matchGuid={MATCHGUID}	
        //    																	&homeTeamId={HOMETEAMID}
		//																		&awayTeamId={AWAYTEAMID}
		//																		&venueId={VENUEID}
		//                 														&matchTitle={MATCHTITLE}
		//                 														&kickOff={KICKOFF} 
		//																		&kickedOffActual={KICKEDOFFACTUAL}
		//                 														&userId={USERID}
		//                 														&creatorId={CREATORID}
		//                 														&sportId={SPORTID}
		//                 														&sharePassword={SHAREPASSWORD}  														    
		//																		&playsDuration={PLAYSDURATION}
		//																		&numberOfPlays={NUMBEROFPLAYS}
		
		String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/Match_AddNew/";
		EMPLOYEE_SERVICE_URI += "?matchGuid=" + matchGUID;
		EMPLOYEE_SERVICE_URI += "&homeTeamId=" + homeTeamId;
		EMPLOYEE_SERVICE_URI += "&awayTeamId=" + awayTeamId;
		EMPLOYEE_SERVICE_URI += "&venueId="    + venueID;
		EMPLOYEE_SERVICE_URI += "&matchTitle=" + matchTitle;
		EMPLOYEE_SERVICE_URI += "&kickOff=" + Globals.FormatDateWCF(scheduledKickOff);
		EMPLOYEE_SERVICE_URI += "&kickedOffActual=" + Globals.FormatDateWCF(actualKickOff);
		EMPLOYEE_SERVICE_URI += "&userId=" + userID;
		EMPLOYEE_SERVICE_URI += "&creatorId=" + creatorID;
		EMPLOYEE_SERVICE_URI += "&sportId=" + sportID;
		EMPLOYEE_SERVICE_URI += "&sharePassword=" + password;
		EMPLOYEE_SERVICE_URI += "&playsDuration=" + playDuration;
		EMPLOYEE_SERVICE_URI += "&numberOfPlays=" + numberOfPlays;
		
		HttpGet request = new HttpGet(EMPLOYEE_SERVICE_URI);

		// set the header to get the data in JSON format
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		// get the response
		HttpResponse response;
		
			response = client.execute(request);
			HttpEntity entity = response.getEntity();
			
			if (entity.getContentLength() != 0) {
				// stream reader object
				Reader employeeReader = new InputStreamReader(response.getEntity().getContent());
				// create a buffer to fill if from reader
				char[] buffer = new char[(int) response.getEntity().getContentLength()];
				// fill the buffer by the help of reader
				employeeReader.read(buffer);
				// close the reader streams
				employeeReader.close();

				// for the employee json object
				// String s = new String(buffer);
				String s = new String(buffer).replaceAll("\"","");
				Globals.NewMatchResponse = new String(s);
			}
			this.notifyAll();
			}
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
	}
	
}
