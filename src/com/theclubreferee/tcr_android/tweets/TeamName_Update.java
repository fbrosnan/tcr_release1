//This class is for communication with the Web Services
//for sending data to the web

package com.theclubreferee.tcr_android.tweets;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import com.theclubreferee.tcr_android.Globals;



//@Frank this will obviously need values from the New Team form & button.
//WCF will not save the teamName if it already exists in the database.

//This function requires a valid TeamNameID to match what's on the Server

	//How to call the function 

	//SaveTeamName stn = new SaveTeamName();
	//stn.TeamId = 10005;
	//stn.TeamName = "Tallaght RFC";
	//stn.Description = "West Dublin";
	//stn.TeamGuid = UUID.randomUUID();
	//new Thread(stn).start();
	
public class TeamName_Update implements Runnable {

	public int TeamId; //<<<<<<< REQUIRED
	public String TeamName;
	public String Description;
	public UUID TeamGuid;
	
	@Override
	public void run() {
		try {

			DefaultHttpClient client = new DefaultHttpClient();
			String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/TeamName_Update/";
			EMPLOYEE_SERVICE_URI += "?teamId=" + TeamId;
			EMPLOYEE_SERVICE_URI += "&teamName=" + TeamName;
			EMPLOYEE_SERVICE_URI += "&teamDescription=" + Description;
			EMPLOYEE_SERVICE_URI += "&teamGuid=" + TeamGuid.toString();
						
			
			//String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/SaveTeamName/";
			HttpGet request = new HttpGet(EMPLOYEE_SERVICE_URI);

			// set the header to get the data in JSON format
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");

			// get the response
			HttpResponse response = client.execute(request);

			HttpEntity entity = response.getEntity();

			// if entity content lenght 0, means no employee exist in the system
			// with these code
			if (entity.getContentLength() != 0) {
				// stream reader object
				Reader employeeReader = new InputStreamReader(response.getEntity().getContent());
				// create a buffer to fill if from reader
				char[] buffer = new char[(int) response.getEntity().getContentLength()];
				// fill the buffer by the help of reader
				employeeReader.read(buffer);
				// close the reader streams
				employeeReader.close();

				// for the employee json object
				// String s = new String(buffer);

				Globals.TeamNameResponse = new String(buffer);
			}

		} catch (Exception e) {
			// any exception show the error layout

			Globals.TeamNameResponse += e.getMessage() + e.getLocalizedMessage();
			

		}
	}

}
