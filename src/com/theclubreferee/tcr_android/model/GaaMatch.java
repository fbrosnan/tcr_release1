package com.theclubreferee.tcr_android.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.List;

import com.theclubreferee.tcr_android.Globals;

public class GaaMatch extends Match {

	 public boolean load(){
		boolean success = false;
		File baseDir = Globals.BaseDir;
		FileInputStream fis = null;
	    ObjectInputStream in = null;
	    Match settings = new GaaMatch();
	    if (baseDir != null && baseDir.exists())
	    {
	    	try {
	    		File file = new File(baseDir,"MatchSettings.ser");
	    		if (file.exists()){
	    			fis =       new FileInputStream(file);
	    			if (fis.available() > 0){
	    				in = new ObjectInputStream(fis);
	    				settings = (GaaMatch) in.readObject();
	    				this.setAwayTeam(settings.getAwayTeam());
	    				this.setGameTitle(settings.getGameTitle());
	    				this.setHomeTeam(settings.getHomeTeam());
	    				this.setKickOffDate(settings.getKickOffDate());
	    				this.setMatchGuid(settings.getMatchGuid());
	    				this.setMatchID(settings.getMatchID());
	    				this.setMatchPeriod(settings.getMatchPeriod());
	    				this.setReferee(settings.getReferee());
	    				this.setSportTypeID(settings.getSportTypeID());
	    				this.setVenue(settings.getVenue());
	    				this.setAnnounced(settings.getAnnounced());
	    				this.setSetUp(settings.isSetUp());
	    				this.setMatchDuration(settings.getMatchDuration());
	    				in.close();
	    				success = true;
	    			}
	    			
	    		}
	    		else {
	    			file.createNewFile();
				
	    		}
	    	} catch (Exception ex) {
	    		ex.printStackTrace();
	    	}
	    }
	    else 
	    {
	    	
	    }
	    return success;
	}

	@Override
	public List<Serializable> getListOfPersistables() {
		// TODO Auto-generated method stub
		return null;
	}
}
