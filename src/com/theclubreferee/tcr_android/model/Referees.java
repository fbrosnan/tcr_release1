/**
 * 
 */
package com.theclubreferee.tcr_android.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.UUID;

import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.WcfConverter;
import com.theclubreferee.tcr_android.backend.BackEndConnection;
import com.theclubreferee.tcr_android.tweets.UserName_AddNew;


/**
 * @author francisbrosnan
 * These is a container for all the referees
 * that are created in the system.
 *
 */
public class Referees extends Observable implements Serializable, Persistable {

	static final long serialVersionUID = -5085645081685734932L;
	private static Referees _instance=null;
	
	public static Referees getInstance()
	{
		if (_instance==null)
		{
			_instance=new Referees();
			_instance.load();
			BackEndConnection.getInstance().addPersistable(_instance);
		}
		return _instance;
	}
	
	private Referees()
	{
		
	}
	
	private ArrayList<Referee> allReferees = new ArrayList<Referee>();
	private Referee            defaultReferee;

	public ArrayList<Referee> getAllReferees() {
		return allReferees;
	}

	public void setAllReferees(ArrayList<Referee> allReferees) {
		this.allReferees = allReferees;
	}
	
	public Referee getDefaultReferee(){
		return defaultReferee;
	}
	
	public void setDefaultReferee(Referee referee){
		defaultReferee = referee;
	}
	
	public Referee getRefereeByName(String firstName, String lastName){
		for (Referee ref : allReferees){
			if (ref.getFirstName().equalsIgnoreCase(firstName) && ref.getLastName().equalsIgnoreCase(lastName)){
				return ref;
			}
		}
		return null;
	}
	
	//Referees stored in file Refs.ser
	public boolean load(){
			boolean success = false;
			File baseDir = Globals.BaseDir;
			FileInputStream fis = null;
		    ObjectInputStream in = null;
		    Referees referees = new Referees();
		    if (baseDir != null && baseDir.exists())
		    {
		    	try {
		    		File file = new File(baseDir,"Referees.ser");
		    		if (file.exists()){
		    			fis = new FileInputStream(file);
		    			if (fis.available() > 0){
		    				in = new ObjectInputStream(fis);
		    				referees = (Referees) in.readObject();
		    				
		    				this.allReferees = referees.allReferees;
		    				this.defaultReferee = referees.defaultReferee;
		    				in.close();
		    				success = true;
		    			}
		    			
		    		}
		    		else {
		    			file.createNewFile();
						success = true;
		    		}
		    	}
		    	catch (Exception ex) {
		    		ex.printStackTrace();
		    	}
		    } else {
		    	
		    }
		   return success; 
	}
	
	public boolean saveToLocalStorage(){
		
		boolean success = false;
		File baseDir = Globals.BaseDir;
		if (baseDir != null && baseDir.exists()){
			FileOutputStream fos = null;
			ObjectOutputStream out = null;
			try {
				
				File file = new File(baseDir,"Referees.ser");
				fos = new FileOutputStream(file);
				out = new ObjectOutputStream(fos);
				out.writeObject(this);
				
				out.close();
				success = true;
				
			} catch (Exception ex) {
				ex.printStackTrace();
				
			}
		}
		else 
		{
			
		}
		setChanged();
		notifyObservers(new SaveType("All referees", success, SaveType.Medium.LOCAL));
		return success;
	}

	@Override
	public boolean saveToNetwork(Serializable object) {
		// TODO Auto-generated method stub
		boolean success=false;
		Referee referee=(Referee)object;
		UserName_AddNew addNewUserRequest = new UserName_AddNew();
		addNewUserRequest.FirstName = WcfConverter.convertToWcfFormat(referee.getFirstName());
		addNewUserRequest.LastName  = WcfConverter.convertToWcfFormat(referee.getLastName());
		addNewUserRequest.UserName  = WcfConverter.convertToWcfFormat(referee.getUserName());
		addNewUserRequest.AppVersion = "1.1.1";
		addNewUserRequest.UserGuid  = referee.getRefereeGuid();
		//TODO where do we get this from
		addNewUserRequest.AppGuid   = UUID.randomUUID();
		new Thread(addNewUserRequest).start();
		
		synchronized (addNewUserRequest){
			try {
				addNewUserRequest.wait(1000);
				if (Globals.isNumeric(Globals.UserNewNameResponse)){
					referee.setRefereeID(Integer.parseInt(Globals.UserNewNameResponse));
				}
				else {
					success=false;
				}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		setChanged();
		notifyObservers(new SaveType(referee.getName(), success, SaveType.Medium.NETWORK));
		return success;
		
	}

	public boolean refereeAlreadyExists(String firstName, String lastName,
			String userName) {
		boolean returnVal = false;

		// TODO Auto-generated method stub
		for (Referee ref : allReferees) {
			if (ref.getFirstName().equalsIgnoreCase(firstName)
					&& ref.getLastName().equalsIgnoreCase(lastName)
					&& ref.getUserName().equalsIgnoreCase(userName)) {
				returnVal = true;
				break;
			}

		}
		return returnVal;
	}

	@Override
	public boolean saveToMemory(Serializable object) {
		// TODO Auto-generated method stub
		allReferees.add((Referee)(object));
		return true;
	}

	@Override
	public List<? extends Serializable> getListOfPersistables() {
		// TODO Auto-generated method stub
		return allReferees;
	}
}