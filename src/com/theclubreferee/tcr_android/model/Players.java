package com.theclubreferee.tcr_android.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.theclubreferee.tcr_android.Globals;

/**
 * 
 * @author francisbrosnan
 * This class holds all Players in a collection ArrayList.
 */
public class Players implements Serializable {
	
	static final long serialVersionUID = -4022498798404625713L;

	private static Players _instance=null;
	
	private List<Player> allPlayers = new ArrayList<Player>();

	public static Players instance()
	{
		if (_instance==null)
		{
			_instance=new Players();
			_instance.load();
		}
		return _instance;
	}
	
	private Players()
	{
		
	}
	public List<Player> getAllPlayers() {
		return allPlayers;
	}

	public void setAllPlayers(List<Player> allPlayers) {
		this.allPlayers = allPlayers;
	}
	
	public boolean doesPlayerExisitAlready(String PlayerName){
		for (Player Player : allPlayers){
			if (Player.getName().equalsIgnoreCase(PlayerName)){
				return true;
			}
		}
		return false;
	}
	
	public Player getPlayerByName(String firstName, String lastName){
		for (Player Player : allPlayers){
			if (Player.getFirstName().equalsIgnoreCase(firstName) && Player.getLastName().equalsIgnoreCase(lastName)){
				return Player;
			}
		}
		return null;
	}
	
	public Player getPlayerByGuid(UUID PlayerGUID){
		for (Player Player : allPlayers){
			if (Player.getPlayerGuid().equals(PlayerGUID)){
				return Player;
			}
		}
		return null;
	}
	
	private boolean load(){
		boolean success = false;
		File baseDir = Globals.BaseDir;
		FileInputStream fis = null;
	    ObjectInputStream in = null;
	    Players Players = new Players();
	    if (baseDir != null && baseDir.exists())
	    {
	    	try {	    		
	    			File file = new File(baseDir,"Players.ser"); 
	    			if (file.exists()){
	    				fis = new FileInputStream(file);	
	    				if (fis.available() > 0){
	    					in = new ObjectInputStream(fis);
	    					Players = (Players) in.readObject();
	    					this.allPlayers = Players.getAllPlayers();
	    					in.close();
	    					success = true;
	    				}
	    			}
	    			else {
	    				file.createNewFile();
	    				success = true;
	    			}
	    		
	    	} catch (Exception ex) {
	    		ex.printStackTrace();
	    	}
	    }
	    else {
	    }
	    return success;
	}
	
	private boolean save(){
		boolean success = false;
		File baseDir = Globals.BaseDir;
		if (baseDir != null && baseDir.exists())
	    {
			
			FileOutputStream fos = null;
			ObjectOutputStream out = null;
			try {
				File file = new File(baseDir,"Players.ser"); 
				fos = new FileOutputStream(file);
				out = new ObjectOutputStream(fos);
				out.writeObject(this);
				out.close();
				success = true;
				}
				
			catch (Exception ex) {
				
				ex.printStackTrace();
			}
	    }
		else {
			
		}
		return success;
	}
	public void addPlayer(Player p) {
		// TODO Auto-generated method stub
		if (!doesPlayerExisitAlready(p.getName()))
		{
			allPlayers.add(p);
		}
		
	}

}
