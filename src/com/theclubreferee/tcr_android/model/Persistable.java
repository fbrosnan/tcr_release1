package com.theclubreferee.tcr_android.model;

import java.io.Serializable;
import java.util.List;

/**
 * A general persist interface where data can be loaded, saved to various
 * media and the stuff to be persisted is in a list.
 * @author Jack
 *
 */
public interface Persistable {
	
	public boolean load();
	
	public boolean saveToMemory(Serializable object);
	public boolean saveToLocalStorage();
	public boolean saveToNetwork(Serializable object);

	public List<? extends Serializable> getListOfPersistables();

}
