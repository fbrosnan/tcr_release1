/**
 * 
 */
package com.theclubreferee.tcr_android.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.theclubreferee.tcr_android.Globals;

/**
 * @author francisbrosnan
 * This class stores all venues in a collection (arrayList)
 */
public class Venues implements Serializable, Persistable {
	
	static final long serialVersionUID = -3401333373345638097L;

	private static Venues _instance=null;
	
	private ArrayList<Venue> allVenues = new ArrayList<Venue>();
	
	public static Venues getInstance()
	{
		if (_instance==null)
		{
			_instance=new Venues();
			_instance.load();
		}
		return _instance;
	}
	
	public ArrayList<Venue> getAllVenues() {
		return allVenues;
	}

	public void setAllVenues(ArrayList<Venue> allVenues) {
		this.allVenues = allVenues;
	}
	
	public Venue getVenueByName(String venueName){
		for (Venue venue : allVenues){
			if (venue.getVenueName().equalsIgnoreCase(venueName)){
				return venue;
			}
		}
		return null;
	}
	
	//Venues stored in file Venues.ser
		public boolean load(){
			boolean success = false;
			File baseDir = Globals.BaseDir;
			Venues venues = new Venues();
			if (baseDir != null && baseDir.exists())
		    {
				FileInputStream fis = null;
				ObjectInputStream in = null;
				try {		
					File file = new File(baseDir,"Venues.ser");
					if (file.exists()){
						fis = new FileInputStream(file);
						if (fis.available() > 0){
							in = new ObjectInputStream(fis);
							venues = (Venues) in.readObject();
							this.setAllVenues(venues.getAllVenues());
							
							in.close();
							fis.close();
							success = true;
						}
						
					} else {
						file.createNewFile();
						
					}
					
				
				} catch (Exception ex) {
					ex.printStackTrace();
				}
		    } else {
		    	
		    }
		    return success;
		}
	
		public boolean saveToLocalStorage(){
		
			boolean success = false;
			File baseDir = Globals.BaseDir;
			if (baseDir != null && baseDir.exists()){
				FileOutputStream fos = null;
				ObjectOutputStream out = null;
				try {
					
					File file = new File(baseDir,"Venues.ser"); 	
					fos = new FileOutputStream(file);
					out = new ObjectOutputStream(fos);
					out.writeObject(this);
					
					out.flush();
					out.close();
					fos.close();
					success = true;
					
				} catch (Exception ex) {
					ex.printStackTrace();
					
				}
		    }
			else {
				
			}
			return success;
		}

		@Override
		public boolean saveToNetwork(Serializable object) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean saveToMemory(Serializable object) {
			// TODO Auto-generated method stub
			allVenues.add((Venue)(object));
			return true;
		}

		@Override
		public List<? extends Serializable> getListOfPersistables() {
			// TODO Auto-generated method stub
			return allVenues;
		}
}
