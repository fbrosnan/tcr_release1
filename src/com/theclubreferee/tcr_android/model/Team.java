package com.theclubreferee.tcr_android.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.theclubreferee.tcr_android.Score;

/**
 * 
 * @author francisbrosnan
 * This class is the data container for team data.
 */
public class Team implements Serializable {
	
	static final long serialVersionUID = -1715127277125863976L;
	
	private int teamID;
	private String teamName;
	private String teamDescription;
	private UUID teamGuid;
	
	public Score score;
	
	public Boolean isHomeTeam;  //Ray.
	
	private List<IPlayer> players=null;
	
	public Team(String teamName2, String teamDescription2, int i,
			UUID randomUUID) {
		this.setTeamName(teamName2);
		this.setTeamDescription(teamDescription2);
		this.setTeamID(i);
		this.setTeamGuid(randomUUID);
		setupPlayers();
		
		//Save locally
		Teams.getInstance().saveToMemory(this);
	}
	
	private void setupPlayers()
	{
		players = new ArrayList<IPlayer>();
		Player p = new GAAPlayer(1, "Peter", "Schmeichel");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(2, "Gary", "Neville");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(3, "Denis", "Irwin");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(4, "Gary", "Pallister");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(5, "Steve", "Bruce");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(6, "Bryan", "Robson");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(7, "Eric", "Cantona");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(8, "Ryan", "Giggs");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(9, "Mark", "Hughes");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(10, "Brian", "McClair");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(11, "Ryan", "Giggs");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(12, "Andy", "Cole");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(13, "Dwight", "Yorke");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(14, "Japp", "Stam");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(15, "Rio", "Ferdinand");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(16, "Roy", "Keane");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(17, "Andrei", "Kanchelskis");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(18, "Lee", "Sharpe");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(19, "Paul", "McGrath");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(20, "Lee", "Martin");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(21, "Jim", "Leighton");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(22, "Edwin", "Van Der Saar");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(23, "Paul", "Ince");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(24, "Robin", "Van Persie");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(25, "Clayton", "Blackmore");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(26, "Paul", "Parker");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(27, "Michael", "Phelan");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(28, "John", "O'Shea");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(29, "Norman", "Whiteside");
		Players.instance().addPlayer(p);
		this.players.add(p);
		p = new GAAPlayer(30, "Bobby", "Charlton");
		Players.instance().addPlayer(p);
		this.players.add(p);

		instantiateScore();
	}

	private void instantiateScore(){
		if(score == null)
			score = new Score();
	}
	
	public int getTeamID() {
		return teamID;
	}
	
	public void setTeamID(int teamID) {
		this.teamID = teamID;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	
	public String getTeamDescription() {
		return teamDescription;
	}

	public void setTeamDescription(String teamDescription) {
		this.teamDescription = teamDescription;
	}

	public UUID getTeamGuid() {
		return teamGuid;
	}

	public void setTeamGuid(UUID teamGuid) {
		this.teamGuid = teamGuid;
	}
	
	@Override
	public String toString() {
		return this.teamName;
	}

	public int getTotalGoals() {
		int returnGoals=0;
		int size=players.size();
		for (int i=0; i<size; i++)
		{
			returnGoals+=((GAAPlayer)(players.get(i))).getGoalCount();
		}
		return returnGoals;
	}
	
	public int getTotalPoints() {
		int returnPoints=0;
		int size=players.size();
		for (int i=0; i<size; i++)
		{
			returnPoints+=((GAAPlayer)(players.get(i))).getPointCount();
		}
		return returnPoints;
	}

	public List<IPlayer> getPlayers() {
		return players;
	}

	public int getTotalYellowCards() {
		int returnYellowCards = 0;
		int size=players.size();
		for (int i=0; i<size; i++)
		{
			returnYellowCards += ((GAAPlayer) (players.get(i)))
					.getYellowCardCount();
		}
		return returnYellowCards;
	}
	
	public int getTotalRedCards() {
		int returnRedCards = 0;
		int size=players.size();
		for (int i=0; i<size; i++){
			returnRedCards += ((GAAPlayer) (players.get(i)))
					.getRedCardCount();
		}
		return returnRedCards;
	}
	
	public int getTotalBlackCards() {
		int returnBlackCards = 0;
		int size=players.size();
		for (int i=0; i<size; i++){
			returnBlackCards += ((GAAPlayer) (players.get(i)))
					.getBlackCardCount();
		}
		return returnBlackCards;
	}

	public IPlayer getPlayerByNumber(int playerNumber) {
		IPlayer returnPlayer=null;
		
		int size = players.size();
		for (int i = 0; i < size; i++) {
			IPlayer tempPlayer = players.get(i);

			if (tempPlayer.getNumber() == playerNumber) {
				returnPlayer = tempPlayer;
				break;
			}
		}
		return returnPlayer;
	}
	
	public void resetTeamScore(){
		int size = players.size();
		for (int i = 0; i < size; i++){
			((GAAPlayer)(players.get(i))).resetGoalCount();
			((GAAPlayer)(players.get(i))).resetPointCount();
			((GAAPlayer)(players.get(i))).resetRedCardCount();
			((GAAPlayer)(players.get(i))).resetYellowCardCount();
			((GAAPlayer)(players.get(i))).resetBlackCardCount();
		}
	}
}
