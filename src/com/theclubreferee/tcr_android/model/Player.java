package com.theclubreferee.tcr_android.model;


abstract class Player extends Participant implements IPlayer
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3615130456914812669L;
	
	private int number;
	

	public int getNumber() {
		return number;
		
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	public Player(int number, String firstName, String lastName)
	{
		super(firstName, lastName);
		setNumber(number);
	}
}
