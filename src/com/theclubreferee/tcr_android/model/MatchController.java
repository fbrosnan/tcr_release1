package com.theclubreferee.tcr_android.model;

import java.util.Calendar;
import java.util.List;
import java.util.Observer;
import java.util.UUID;

import com.theclubreferee.tcr_android.model.Match.MatchPeriod;
import com.theclubreferee.tcr_android.model.Match.SportTypes;

public class MatchController {
	private static MatchController instance_ = null;
	
	private Match match = null; 
	public static MatchController getInstance()
	{
		if (instance_ == null)
		{
			instance_ = new MatchController();
		}
		
		return instance_;
	}
	
	private MatchController()
	{
		match = new GaaMatch();
		
		//TODO:Needed at this point maybe?
		match.load();
	}
	
	public void addObserver(Observer observer)
	{
		match.addObserver(observer);
	}

	public void setMatchPeriod(MatchPeriod firsthalf) {
		// TODO Auto-generated method stub
		match.setMatchPeriod(firsthalf);
	}

	public int getMatchDuration() {
		// TODO Auto-generated method stub
		return match.getMatchDuration();
	}

	public UUID getHomeTeamGuid() {
		// TODO Auto-generated method stub
		return match.getHomeTeam().getTeamGuid();
	}
	
	public UUID getAwayTeamGuid() {
		// TODO Auto-generated method stub
		return match.getAwayTeam().getTeamGuid();
	}

	public int getMatchID() {
		// TODO Auto-generated method stub
		return match.getMatchID();
	}

	public boolean isSetUp() {
		// TODO Auto-generated method stub
		return match.isSetUp();
	}

	public MatchPeriod getMatchPeriod() {
		// TODO Auto-generated method stub
		return match.getMatchPeriod();
	}

	public String getHomeTeamName() {
		// TODO Auto-generated method stub
		String returnVal = null;
		Team homeTeam = match.getHomeTeam();
		if (homeTeam != null) {
			returnVal = homeTeam.getTeamName();
		}
		return returnVal;
	}
	
	public String getAwayTeamName() {
		// TODO Auto-generated method stub
		String returnVal = null;
		Team awayTeam = match.getAwayTeam();
		if (awayTeam != null) {
			returnVal = awayTeam.getTeamName();
		}
		return returnVal;
	}

//	public void setHomeTeamName(String teamByName) {
//		// TODO Auto-generated method stub
//		match.getHomeTeam().setTeamName(teamByName);
//		
//	}
//	
//	public void setAwayTeamName(String teamByName) {
//		// TODO Auto-generated method stub
//		match.getAwayTeam().setTeamName(teamByName);
//		
//	}

	public void resetMatch() {
		// TODO Auto-generated method stub
		//TODO: Is it necessary to create a new GAA match?
		match=null;
		match = new GaaMatch();
		match.setAnnounced(false);
		match.setSetUp(false);
		match.saveToLocalStorage();
		
	}

	public int getHomeTeamTotalGoals() {
		// TODO Auto-generated method stub
		return match.getHomeTeam().getTotalGoals();
	}
	
	public int getAwayTeamTotalGoals() {
		// TODO Auto-generated method stub
		return match.getAwayTeam().getTotalGoals();
	}

	public int getHomeTeamTotalPoints() {
		// TODO Auto-generated method stub
		return match.getHomeTeam().getTotalPoints();
	}
	
	public int getAwayTeamTotalPoints() {
		// TODO Auto-generated method stub
		return match.getAwayTeam().getTotalPoints();
	}

	public int getHomeTeamTotalYellowCards() {
		// TODO Auto-generated method stub
		return match.getHomeTeam().getTotalYellowCards();
	}
	
	public int getAwayTeamTotalYellowCards() {
		// TODO Auto-generated method stub
		return match.getAwayTeam().getTotalYellowCards();
	}
	
	public int getHomeTeamTotalRedCards() {
		// TODO Auto-generated method stub
		return match.getHomeTeam().getTotalRedCards();
	}
	
	public int getAwayTeamTotalRedCards() {
		// TODO Auto-generated method stub
		return match.getAwayTeam().getTotalRedCards();
	}
	
	public int getHomeTeamTotalBlackCards() {
		// TODO Auto-generated method stub
		return match.getHomeTeam().getTotalBlackCards();
	}
	
	public int getAwayTeamTotalBlackCards() {
		// TODO Auto-generated method stub
		return match.getAwayTeam().getTotalBlackCards();
	}

	public int getHomeTeamID() {
		// TODO Auto-generated method stub
		return match.getHomeTeam().getTeamID();
	}
	
	public int getAwayTeamID() {
		// TODO Auto-generated method stub
		return match.getAwayTeam().getTeamID();
	}

	public List<IPlayer> getHomeTeamPlayers() {
		// TODO Auto-generated method stub
		return match.getHomeTeam().getPlayers();
	}
	
	public List<IPlayer> getAwayTeamPlayers() {
		// TODO Auto-generated method stub
		return match.getAwayTeam().getPlayers();
	}

	public IPlayer getHomeTeamPlayerByNumber(int playerNumber) {
		// TODO Auto-generated method stub
		return match.getHomeTeam().getPlayerByNumber(playerNumber);
	}
	
	public IPlayer getAwayTeamPlayerByNumber(int playerNumber) {
		// TODO Auto-generated method stub
		return match.getAwayTeam().getPlayerByNumber(playerNumber);
	}

	public void incrementYellowCardCountForAwayPlayer(int playerNumber) {
		// TODO Auto-generated method stub
		((GAAPlayer)(match.getAwayTeam().getPlayerByNumber(playerNumber))).incrementYellowCardCount();
	}
	
	public void incrementYellowCardCountForHomePlayer(int playerNumber) {
		// TODO Auto-generated method stub
		((GAAPlayer)(match.getHomeTeam().getPlayerByNumber(playerNumber))).incrementYellowCardCount();
	}
	
	public void incrementRedCardCountForAwayPlayer(int playerNumber) {
		// TODO Auto-generated method stub
		((GAAPlayer)(match.getAwayTeam().getPlayerByNumber(playerNumber))).incrementRedCardCount();
	}
	
	public void incrementRedCardCountForHomePlayer(int playerNumber) {
		// TODO Auto-generated method stub
		((GAAPlayer)(match.getHomeTeam().getPlayerByNumber(playerNumber))).incrementRedCardCount();
	}
	
	public void incrementBlackCardCountForAwayPlayer(int playerNumber) {
		// TODO Auto-generated method stub
		((GAAPlayer)(match.getAwayTeam().getPlayerByNumber(playerNumber))).incrementBlackCardCount();
	}
	
	public void incrementBlackCardCountForHomePlayer(int playerNumber) {
		// TODO Auto-generated method stub
		((GAAPlayer)(match.getHomeTeam().getPlayerByNumber(playerNumber))).incrementBlackCardCount();
	}
	
	public void incrementGoalCountForAwayPlayer(int playerNumber) {
		// TODO Auto-generated method stub
		((GAAPlayer)(match.getAwayTeam().getPlayerByNumber(playerNumber))).incrementGoalCount();
	}
	
	public void incrementGoalCountForHomePlayer(int playerNumber) {
		// TODO Auto-generated method stub
		((GAAPlayer)(match.getHomeTeam().getPlayerByNumber(playerNumber))).incrementGoalCount();
	}
	
	public void incrementPointCountForAwayPlayer(int playerNumber) {
		// TODO Auto-generated method stub
		((GAAPlayer)(match.getAwayTeam().getPlayerByNumber(playerNumber))).incrementPointCount();
	}
	
	public void incrementPointCountForHomePlayer(int playerNumber) {
		// TODO Auto-generated method stub
		((GAAPlayer)(match.getHomeTeam().getPlayerByNumber(playerNumber))).incrementPointCount();
	}

	public Calendar getKickOffDate() {
		// TODO Auto-generated method stub
		return match.getKickOffDate();
	}

	public String getGameTitle() {
		// TODO Auto-generated method stub
		return match.getGameTitle();
	}

	public Venue getVenue() {
		// TODO Auto-generated method stub
		return match.getVenue();
	}

	public boolean isVenueSetup() {
		// TODO Auto-generated method stub
		return match.getVenue() != null;
	}

	public String getVenueName() {
		// TODO Auto-generated method stub
		return match.getVenue().getVenueName();
	}

	public UUID getRefereeGuid() {
		// TODO Auto-generated method stub
		return match.getReferee().getRefereeGuid();
	}

	public void setMatchGuid(UUID randomUUID) {
		// TODO Auto-generated method stub
		match.setMatchGuid(randomUUID);
	}

	public UUID getMatchGuid() {
		// TODO Auto-generated method stub
		return match.getMatchGuid();
	}

	public int getRefereeID() {
		// TODO Auto-generated method stub
		return match.getReferee().getRefereeID();
	}

	
	public void save() {
		// TODO Auto-generated method stub
		//TODO:Saving the model.. should we need to call it explcitly?
		match.saveToLocalStorage();
		
	}

	public void setGameTitle(String string) {
		// TODO Auto-generated method stub
		match.setGameTitle(string);
		
	}

	public void setSetUp(boolean b) {
		// TODO Auto-generated method stub
		match.setSetUp(true);
		
	}

	public void setKickOffDate(Calendar kickOffDetails) {
		// TODO Auto-generated method stub
		match.setKickOffDate(kickOffDetails);
		
	}

	public void setMatchID(int parseInt) {
		// TODO Auto-generated method stub
		match.setMatchID(parseInt);
		
	}

	public void setMatchDuration(int parseInt) {
		// TODO Auto-generated method stub
		match.setMatchDuration(parseInt);
		
	}

	public void setReferee(String refereeFirstName, String refereeLastName) {
		// TODO Auto-generated method stub
		match.setReferee(Referees.getInstance().getRefereeByName(refereeFirstName, refereeLastName));
		
	}

	public void setSportTypeID(SportTypes gameTypeValue) {
		// TODO Auto-generated method stub
		match.setSportTypeID(gameTypeValue);
		
	}

	public void setVenue(String venueName) {
		// TODO Auto-generated method stub
		match.setVenue(Venues.getInstance().getVenueByName(venueName));
		
	}

	public void setHomeTeam(String teamName) {
		// TODO Auto-generated method stub
		match.setHomeTeam(Teams.getInstance().getTeamByName(teamName));
		
	}
	
	public void setAwayTeam(String teamName) {
		// TODO Auto-generated method stub
		match.setAwayTeam(Teams.getInstance().getTeamByName(teamName));
		
	}
	
	public Team getHomeTeam(){
		return match.getHomeTeam();
	}
	
	public Team getAwayTeam(){
		return match.getAwayTeam();
	}
}
