package com.theclubreferee.tcr_android.model;

public interface IGAAPlayer {

	int getGoalCount();

	int getPointCount();

	int getBlackCardCount();

	int getYellowCardCount();

	int getRedCardCount();

}
