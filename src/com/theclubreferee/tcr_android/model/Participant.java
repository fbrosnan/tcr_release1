package com.theclubreferee.tcr_android.model;

import java.io.Serializable;
import java.util.UUID;

public abstract class Participant implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1497469308917981146L;

	private String firstName;
	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	private String lastName;
	
	private UUID participantGuid;

	public Participant() {
		this("","");
	}

	public Participant(String firstName, String lastName) {
		setFirstName(firstName);
		setLastName(lastName);
		participantGuid=UUID.randomUUID();
	}
	
	public String getName() {
		return firstName + " " + lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName == null ? "" : firstName;	
	}
	
	public void setLastName(String lastName)
	{
		this.lastName = lastName == null ? "" : lastName;
	}
	
	public UUID getPlayerGuid() {
		return participantGuid;
	}

	public void setPlayerGuid(UUID playerGuid) {
		this.participantGuid = playerGuid;
	}
	


}
