/**
 * 
 */
package com.theclubreferee.tcr_android.model;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author francisbrosnan
 * This class holds all data to do with venues.
 */
public class Venue implements Serializable {
	
	static final long serialVersionUID = -6275661986828871506L;
	
	private int venueID;
	private String venueName;
	private String cityPlaceID;
	private String country;
	private String googleMap;
	private UUID   venueGuid;

	private static int venueIDCounter=0;
	
	public Venue(){
		venueName = "";
		this.venueGuid = UUID.randomUUID();
	}
	
	public Venue(String venueName, String cityPlaceID, String country){
		this.venueName = venueName;
		this.cityPlaceID = cityPlaceID;
		this.country=country;
		this.venueGuid = UUID.randomUUID();
		this.venueID = venueIDCounter++;
		
		Venues.getInstance().saveToLocalStorage();
	}
	
	
	public int getVenueID() {
		return venueID;
	}
	
	public void setVenueID(int venueID) {
		this.venueID = venueID;
	}
	
	public String getVenueName() {
		return venueName;
	}
	
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public String getCityPlaceID() {
		return cityPlaceID;
	}

	public void setCityPlaceID(String cityPlaceID) {
		this.cityPlaceID = cityPlaceID;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getGoogleMap() {
		return googleMap;
	}

	public void setGoogleMap(String googleMap) {
		this.googleMap = googleMap;
	}

	public UUID getVenueGuid() {
		return venueGuid;
	}

	public void setVenueGuid(UUID venueGuid) {
		this.venueGuid = venueGuid;
	}

}
