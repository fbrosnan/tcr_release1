/**
 * 
 */
package com.theclubreferee.tcr_android.model;

import java.util.UUID;

/**
 * @author francisbrosnan
 * This class holds data for each referee in the system.
 */
public class Referee extends Participant {
	
	static final long serialVersionUID = 665025949125690413L;
	
	private int 	refereeID;
	private UUID 	refereeGuid;
	private UUID 	dnnGuid;
	
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	private static int refIDCounter=0;
	
	public Referee(String firstName, String lastName, String userName, boolean isDefault)
	{
		super(firstName, lastName);
		this.userName=userName;
		this.refereeID=refIDCounter++;
		this.refereeGuid = UUID.randomUUID();
		this.dnnGuid = UUID.randomUUID();
		
		
		
		//Save to network
		Referees.getInstance().saveToMemory(this);
		//Referees.getInstance().saveToNetwork(this);
		
		//Save locally
		//Referees.getInstance().saveToLocalStorage();
		if (isDefault)
		{
			Referees.getInstance().setDefaultReferee(this);
		}
	}
	
	public int getRefereeID() {
		return refereeID;
	}
	
	public void setRefereeID(int refereeID) {
		this.refereeID = refereeID;
	}
	
	public UUID getRefereeGuid() {
		return refereeGuid;
	}
	
	public void setRefereeGuid(UUID refereeGuid) {
		this.refereeGuid = refereeGuid;
	}

	public UUID getDnnGuid() {
		return dnnGuid;
	}

	public void setDnnGuid(UUID dnnGuid) {
		this.dnnGuid = dnnGuid;
	}

	

}
