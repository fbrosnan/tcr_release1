package com.theclubreferee.tcr_android.model;

/**
 * @author francisbrosnan
 * This class is the data container for MatchSettings so it stores a 
 * match setup .It is created when the user initiates the match setup dialog and
 * removed when the match is finished.
 */
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import java.util.UUID;

import com.theclubreferee.tcr_android.Globals;

public abstract class Match extends Observable implements Serializable, Persistable {
	
	static final long serialVersionUID = 9215211839358247970L;

	//example: http://javarevisited.blogspot.ie/2011/08/enum-in-java-example-tutorial.html
	//Ray - Added enum of SportTypes + Base Values
	public enum SportTypes {
		GaaFootball(1000), Hurling(2000), Camogie(3000), Union(4000), League(5000), Tag(6000),  Soccer(7000), Football(8000);

		 public int value;

		private SportTypes(int value) {
			this.value = value;
		}

	}
	
	Match()
	{
		
	}
	
	private boolean isSetUp;
	
	 void setSetUp(boolean isSetUp){
		this.isSetUp = isSetUp;
		setChanged();notifyObservers();
	}
	
	 boolean isSetUp(){
		return isSetUp;
	}
	
	
	private boolean isAnnounced;
	
	 void setAnnounced(boolean isAnnounced){
		this.isAnnounced = isAnnounced;
		setChanged();notifyObservers();
	}
	
	 boolean getAnnounced(){
		return isAnnounced;
	}

	// CurrentPeriodID
	 public enum MatchPeriod {
		MatchNotStarted, FirstHalf, HalfTime, SecondHalf, MatchFinished
	}

	// AppId

	// MatchID
	private int matchID;

	 int getMatchID() {
		return matchID;
	}

	 void setMatchID(int matchID) {
		this.matchID = matchID;
		setChanged();notifyObservers();
	}

	// MatchGuid
	private UUID matchGuid;
	 UUID getMatchGuid() {
		return matchGuid;
	}

	 void setMatchGuid(UUID matchGuid) {
		this.matchGuid = matchGuid;
		setChanged();notifyObservers();
	}
	
	private Team homeTeam;
	

	private Team awayTeam;
	

	
	private Venue venue;
	

	// Game Title
	private String gameTitle;

	 String getGameTitle() {
		return gameTitle;
	}

	 void setGameTitle(String gameTitle) {
		this.gameTitle = gameTitle;
		setChanged();notifyObservers();
	}

	// KickOffDate +
	// KickOffTime
	private Calendar kickOffDate;

	 Calendar getKickOffDate() {
		return kickOffDate;
	}

	 void setKickOffDate(Calendar kickOffDate) {
		this.kickOffDate = kickOffDate;
		setChanged();notifyObservers();
	}

	// RefereeGuid
//	private UUID refereeGuid;
//
//	 UUID getRefereeGuid() {
//		return refereeGuid;
//	}
//
//	 void setRefereeGuid(UUID refereeGuid) {
//		this.refereeGuid = refereeGuid;
//	}
	
	private Referee referee;
	
	// SportTypeID
	//Ray: Updated from String to Enum
	private SportTypes sportTypeID;
	 SportTypes getSportTypeID() {
		return sportTypeID;
	}
	 void setSportTypeID(SportTypes sportTypeID) {
		this.sportTypeID = sportTypeID;
		setChanged();notifyObservers();
	}

	
	// Current MatchTime
	private MatchPeriod matchPeriod;

	 MatchPeriod getMatchPeriod() {
		return matchPeriod;
	}

	 void setMatchPeriod(MatchPeriod matchPeriod) {
		this.matchPeriod = matchPeriod;
		setChanged();notifyObservers();
	}
	
	private int matchDuration;
	
	 void setMatchDuration(int matchDuration){
		this.matchDuration = matchDuration;
		setChanged();notifyObservers();
	}
	
	 int getMatchDuration(){
		return matchDuration;
	}
	
	 public abstract boolean load();
	
	
	 public boolean saveToLocalStorage(){
		boolean success = false;
		File baseDir = Globals.BaseDir;
		if (baseDir != null && baseDir.exists()){
		
			FileOutputStream fos = null;
			ObjectOutputStream out = null;
			try {
				File file = new File(baseDir,"MatchSettings.ser");
				fos = new FileOutputStream(file);
				out = new ObjectOutputStream(fos);
				out.writeObject(this);
				out.close();
				success = true;
				
			} catch (Exception ex) {
				ex.printStackTrace();
				
			}
		}
		else {
			
		}
		return success;
	}
	 
	 @Override
		public boolean saveToNetwork(Serializable object) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean saveToMemory(Serializable object) {
			// TODO Auto-generated method stub
			return false;
		}
	 
	List<IPlayer> getHomeTeamPlayers()
	{
		return homeTeam.getPlayers();
	}

	
	private void setup(Team team, String teamDescription, UUID teamGuid,
			int teamID, String teamName, boolean isHomeTeam) {
		// TODO Auto-generated method stub
		team.setTeamDescription(teamDescription);
		team.setTeamGuid(teamGuid);
		team.setTeamID(teamID);
		team.setTeamName(teamName);
		team.isHomeTeam = isHomeTeam;
	}


	
	void setHomeTeam(Team team)
	{
		this.homeTeam=team;
		
		setChanged();notifyObservers();
	}
	
	void setAwayTeam(Team team)
	{
		this.awayTeam=team;
		setChanged();notifyObservers();
	}
	
	Team getHomeTeam()
	{
		return homeTeam;
	}
	
	Team getAwayTeam()
	{
		return awayTeam;
	}

	Venue getVenue() {
		// TODO Auto-generated method stub
		return venue;
	}

	Referee getReferee() {
		// TODO Auto-generated method stub
		return referee;
	}
	
	void setVenue(Venue venue) {
		// TODO Auto-generated method stub
		setChanged();
		notifyObservers();
		
	}

	void setReferee(Referee referee) {
		// TODO Auto-generated method stub
		this.referee=referee;
		setChanged();
		notifyObservers();
		
	}

}