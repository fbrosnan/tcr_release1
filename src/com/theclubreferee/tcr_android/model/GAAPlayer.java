package com.theclubreferee.tcr_android.model;

class GAAPlayer extends Player implements IGAAPlayer {

	public GAAPlayer(int number, String firstName, String lastName) {
		super(number, firstName, lastName);
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 2251157032084077181L;
	
	private int blackCardCount;
	private int yellowCardCount;
	private int redCardCount;
	private int pointCount;
	public int getPointCount() {
		return pointCount;
	}
	public void incrementPointCount() {
		this.pointCount++;
	}
	public int getGoalCount() {
		return goalCount;
	}
	public void incrementGoalCount() {
		this.goalCount++;
	}
	private int goalCount;
	public int getBlackCardCount() {
		return blackCardCount;
	}
	public void incrementBlackCardCount() {
		this.blackCardCount++;
	}
	public int getYellowCardCount() {
		return yellowCardCount;
	}
	public void incrementYellowCardCount() {
		this.yellowCardCount++;
	}
	public int getRedCardCount() {
		return redCardCount;
	}
	public void incrementRedCardCount() {
		this.redCardCount++;
	}
	
	public void resetGoalCount(){
		this.goalCount = 0;
	}
	
	public void resetPointCount(){
		this.pointCount = 0;
	}
	
	public void resetYellowCardCount(){
		this.yellowCardCount = 0;
	}
	
	public void resetRedCardCount(){
		this.redCardCount = 0;
	}
	
	public void resetBlackCardCount(){
		this.blackCardCount = 0;
	}
	
	public String toString()
	{
		return getNumber() + " (" + getGoalCount() + "-" + getPointCount() + ")";
	}
	
	

}
