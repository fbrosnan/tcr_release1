package com.theclubreferee.tcr_android.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.UUID;

import com.theclubreferee.tcr_android.Globals;
import com.theclubreferee.tcr_android.WcfConverter;
import com.theclubreferee.tcr_android.backend.BackEndConnection;
import com.theclubreferee.tcr_android.tweets.TeamName_AddNew;

/**
 * 
 * @author francisbrosnan
 * This class holds all teams in a collection ArrayList.
 */
public class Teams extends Observable implements Serializable, Persistable {
	
	static final long serialVersionUID = -4022498798404625713L;

	private static Teams _instance=null;
	
	private List<Team> allTeams = new ArrayList<Team>();

	private Teams()
	{
		
	}
	
	public static Teams getInstance()
	{
		if (_instance==null)
		{
			_instance=new Teams();
			_instance.load();
			BackEndConnection.getInstance().addPersistable(_instance);
		}
		return _instance;
	}
	public List<Team> getAllTeams() {
		return allTeams;
	}

	public void setAllTeams(List<Team> allTeams) {
		this.allTeams = allTeams;
	}
	
	public boolean doesTeamExisitAlready(String teamName){
		for (Team team : allTeams){
			if (team.getTeamName().equalsIgnoreCase(teamName)){
				return true;
			}
		}
		return false;
	}
	
	public Team getTeamByName(String teamName){
		for (Team team : allTeams){
			if (team.getTeamName().equalsIgnoreCase(teamName)){
				return team;
			}
		}
		return null;
	}
	
	public Team getTeamByGuid(UUID teamGUID){
		for (Team team : allTeams){
			if (team.getTeamGuid().equals(teamGUID)){
				return team;
			}
		}
		return null;
	}
	
	public boolean load(){
		boolean success = false;
		File baseDir = Globals.BaseDir;
		FileInputStream fis = null;
	    ObjectInputStream in = null;
	    Teams teams = new Teams();
	    if (baseDir != null && baseDir.exists())
	    {
	    	try {	    		
	    			File file = new File(baseDir,"Teams.ser"); 
	    			if (file.exists()){
	    				fis = new FileInputStream(file);	
	    				if (fis.available() > 0){
	    					in = new ObjectInputStream(fis);
	    					teams = (Teams) in.readObject();
	    					this.allTeams = teams.getAllTeams();
	    					in.close();
	    					success = true;
	    				}
	    			}
	    			else {
	    				file.createNewFile();
	    				success = true;
	    			}
	    		
	    	} catch (Exception ex) {
	    		ex.printStackTrace();
	    	}
	    }
	    else {
	    }
	    return success;
	}
	
	public boolean saveToLocalStorage(){
		
		boolean success = false;
		File baseDir = Globals.BaseDir;
		if (baseDir != null && baseDir.exists())
	    {
			
			FileOutputStream fos = null;
			ObjectOutputStream out = null;
			try {
				File file = new File(baseDir,"Teams.ser"); 
				fos = new FileOutputStream(file);
				out = new ObjectOutputStream(fos);
				out.writeObject(this);
				out.close();
				success = true;
				}
				
			catch (Exception ex) {
				
				ex.printStackTrace();
			}
	    }
		else {
			
		}
		setChanged();
		notifyObservers(new SaveType("All teams", success, SaveType.Medium.LOCAL));
		return success;
	}
	
	public boolean saveToNetwork(Serializable serializable)
	{
		Team theTeam = (Team)serializable;
		boolean success=false;
		TeamName_AddNew addNewTeamRequest = new TeamName_AddNew();
		addNewTeamRequest.Description = WcfConverter.convertToWcfFormat(theTeam.getTeamDescription());
		addNewTeamRequest.TeamGuid    = theTeam.getTeamGuid();
		addNewTeamRequest.TeamName    = WcfConverter.convertToWcfFormat(theTeam.getTeamName());
		new Thread(addNewTeamRequest).start();
		
		synchronized (addNewTeamRequest){
			
			try {
				addNewTeamRequest.wait(1000);
				//ERROR
				if (!Globals.isNumeric(Globals.TeamNameResponse)){
					success=false;
				}
				else {
					theTeam.setTeamID(Integer.parseInt(Globals.TeamNameResponse));
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			setChanged();
			notifyObservers(new SaveType(theTeam.getTeamName(), success, SaveType.Medium.NETWORK));
		}
		return success;
	}

	@Override
	public boolean saveToMemory(Serializable object) {
		// TODO Auto-generated method stub
		allTeams.add((Team)object);
		return true;
	}

	@Override
	public List<? extends Serializable> getListOfPersistables() {
		// TODO Auto-generated method stub
		return allTeams;
	}

}
