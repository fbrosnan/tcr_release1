package com.theclubreferee.tcr_android.model;

public class SaveType {
	
	public enum Medium {LOCAL, NETWORK};
	
	private boolean success;
	private Medium medium;
	private String item;
	
	public SaveType(String item, boolean success, Medium medium)
	{
		this.success=success;
		this.medium=medium;
		this.item=item;
	}

	public Medium getStorageType() {
		// TODO Auto-generated method stub
		return medium;
	}

	public boolean wasSuccessful() {
		// TODO Auto-generated method stub
		return success;
	}
	
	public String getItem() {
		// TODO Auto-generated method stub
		return item;
	}
}