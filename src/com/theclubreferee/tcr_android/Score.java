/**
 * 
 */
package com.theclubreferee.tcr_android;

import java.io.Serializable;

import com.theclubreferee.tcr_android.model.Team;

/**
 * @author Ray Brennan
 * Manages scoring for Teams
 */
public class Score implements Serializable {

	static final long serialVersionUID = -64945827395109829L;
	
	private int goals;
	//increase goals by value
	public void addGoals(int goals){
		this.goals += goals;
	}
	//decrease goals by value
	public void subtractGoals(int goals){
		this.goals -= goals;
	}
	//This allows resetting of the score
	public void setGoals(int goals){
		this.goals = goals;
	}
	public int getGoals(){
		return goals;
	}
	
	
	private int points;
	//increase goals by value
	public void addPoints(int points){
		this.points += points;
	}
	//decrease goals by value
	public void subtractPoints(int points){
		this.points -= points;
	}
	//This allows resetting of the score
	public void setPoints(int points){
		this.points = points;
	}
	public int getPoints(){
		return points;
	}
	
	
	
	//returns a handy string of goals and points like "2-3"
	public String getGaaScore(){
		return String.valueOf(goals) + "-" + String.valueOf(points); 
	}
	
	
	
	//works out the score from all matchEvents
	public void calculateScore(Team t){
		this.setGoals(getGaaGoals(t));
		this.setPoints(getGaaPoints(t));
		
	}
	
	private int getGaaGoals(Team t) {
		int i = 0;

		for (MatchEvent ev : Globals.AllMatchEvents.getAllMatchEvents())
			if (ev.getEventType() == MatchEvent.EventTypes.GaaGoal & ev.teamGuid == t.getTeamGuid())
				i++;

		return i;
	}

	private int getGaaPoints(Team t) {
		int i = 0;

		for (MatchEvent ev : Globals.AllMatchEvents.getAllMatchEvents())
			if (ev.getEventType() == MatchEvent.EventTypes.GaaPoint & ev.teamGuid == t.getTeamGuid())
				i++;

		return i;
	}
	
	
}
