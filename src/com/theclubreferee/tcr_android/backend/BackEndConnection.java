package com.theclubreferee.tcr_android.backend;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.theclubreferee.tcr_android.MatchEvent;
import com.theclubreferee.tcr_android.model.Match;
import com.theclubreferee.tcr_android.model.Persistable;
import com.theclubreferee.tcr_android.model.Referee;
import com.theclubreferee.tcr_android.model.Team;

public class BackEndConnection extends Thread implements IBackEndConnection, Runnable {

	private static IBackEndConnection _instance = null;
	private List<Persistable> persistables_ = null;

	private BackEndConnection() {
		persistables_ = new ArrayList<Persistable>();
		start();
	}

	@Override
	public boolean isConnectionOn() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isServerUp() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void sendBackLog(List<Match> backLogMatch,
			List<MatchEvent> backlogMatchEvents) {
		// TODO Auto-generated method stub

	}

	@Override
	public int newMatch(Match match) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateMatch(Match match, int matchID) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int newUser(Referee ref) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateUser(Referee ref, int refID) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int newTeam(Team team) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateTeam(Team team, int teamID) {
		// TODO Auto-generated method stub

	}

	@Override
	public int newMatchEvent(MatchEvent event) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateMatchEvent(MatchEvent event, int eventID) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteMatchEvent(int eventID) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static IBackEndConnection getInstance() {
		// TODO Auto-generated method stub
		if (_instance == null) {
			_instance = new BackEndConnection();
		}
		return _instance;
	}

	@Override
	public void addPersistable(Persistable _instance) {
		// TODO Auto-generated method stub
		persistables_.add(_instance);
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {

			}

			synchronized (persistables_) {
				for (int i = 0; i < persistables_.size(); i++) {
					persistables_.get(i).saveToLocalStorage();

					List<? extends Serializable> elements = persistables_
							.get(i).getListOfPersistables();
					for (int j = 0; j < elements.size(); j++) {
						persistables_.get(i).saveToNetwork(elements.get(j));
					}
				}
			}

		}
	}

}
