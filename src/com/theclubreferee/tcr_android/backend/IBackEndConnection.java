package com.theclubreferee.tcr_android.backend;

import java.util.List;

import com.theclubreferee.tcr_android.MatchEvent;
import com.theclubreferee.tcr_android.model.Match;
import com.theclubreferee.tcr_android.model.Persistable;
import com.theclubreferee.tcr_android.model.Referee;
import com.theclubreferee.tcr_android.model.Team;



public interface IBackEndConnection {
	
	/*
	 * returns true if we have some form of data connection
	 * switched on.
	 */
	public boolean isConnectionOn();
	
	/*
	 * returns true if the server has responded positely to 
	 * a test message. Note it would be good if we had a dedicated
	 * little ping msg for this. But for now we could do it with
	 * a dummy msg 
	 */
	public boolean isServerUp();
	
	/*
	 * called when a connection comes back up
	 * to upload results. Note Mayby a common super class for 
	 * all data MatchData would be good to have the one list
	 */
	public void sendBackLog(List<Match>         backLogMatch,
			                List<MatchEvent>    backlogMatchEvents);
	
	/*
	 * The rest api. can be used when we know there is a good connection.
	 * see package tweets. int return codes indicating success or failure.
	 */
	public int newMatch(Match match);
	public int updateMatch(Match match,int matchID);
	public int newUser(Referee ref);
	public int updateUser(Referee ref, int refID);
	public int newTeam(Team team);
	public void updateTeam(Team team, int teamID);
	public int newMatchEvent(MatchEvent event);
	public int updateMatchEvent(MatchEvent event, int eventID);
	public int deleteMatchEvent(int eventID);

	public void addPersistable(Persistable _instance);
	
	
}
