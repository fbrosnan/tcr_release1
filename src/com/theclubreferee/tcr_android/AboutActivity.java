package com.theclubreferee.tcr_android;

import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AboutActivity extends Activity {

	private final static String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/EmployeeInfo.svc/GetEmployee/?key=";
	//private final static String EMPLOYEE_SERVICE_URI = "http://www.theclubreferee.com/TcrService.svc?wsdl";

	private EditText evEmployeeId;
	private TextView tvEmployeeCode;
	private TextView tvName;
	private TextView tvAddress;
	private TextView tvBloodGroup;
	private Button bSearchEmployee;
	private LinearLayout linearLayoutEmp;
	private LinearLayout linearLayoutError;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);

		evEmployeeId = (EditText) findViewById(R.id.evEmployeeCode);

		tvEmployeeCode = (TextView) findViewById(R.id.evEmployeeCode);
		tvName = (TextView) findViewById(R.id.tvEmployeeName);
		tvAddress = (TextView) findViewById(R.id.tvAddress);
		tvBloodGroup = (TextView) findViewById(R.id.tvBloodGroup);

		//linearLayoutEmp =
		// (LinearLayout)findViewById(R.id.lLayoutEmployeeInfo);
		// linearLayoutError = (LinearLayout)findViewById(R.id.lLayoutError);

		//linearLayoutEmp.setVisibility(View.GONE);
		//linearLayoutError.setVisibility(View.GONE);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_about, menu);
		return true;
	}

	public void onSearchClick(View view) {
		try {

			DefaultHttpClient client = new DefaultHttpClient();
			// http get request
			HttpGet request = new HttpGet(EMPLOYEE_SERVICE_URI + 11);// evEmployeeId.getText());

			// set the hedear to get the data in JSON formate
			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");

			// get the response
			HttpResponse response = client.execute(request);

			HttpEntity entity = response.getEntity();

			// if entity contect lenght 0, means no employee exist in the system
			// with these code
			if (entity.getContentLength() != 0) {
				// stream reader object
				Reader employeeReader = new InputStreamReader(response
						.getEntity().getContent());
				// create a buffer to fill if from reader
				char[] buffer = new char[(int) response.getEntity()
						.getContentLength()];
				// fill the buffer by the help of reader
				employeeReader.read(buffer);
				// close the reader streams
				employeeReader.close();

				// for the employee json object
				JSONObject employee = new JSONObject(new String(buffer));

				// set the text of text view
				tvEmployeeCode.setText("Code: "
						+ employee.getString("EmployeeId"));
				tvName.setText("Name: " + employee.getString("FirstName") + " "
						+ employee.getString("LastName"));
				tvAddress.setText("Address: " + employee.getString("Address"));
				tvBloodGroup.setText("Blood Group: "
						+ employee.getString("BloodGroup"));

				// show hide layout
				linearLayoutError.setVisibility(View.GONE);
				linearLayoutEmp.setVisibility(View.VISIBLE);
			} else {
				//linearLayoutEmp.setVisibility(View.GONE);
				//linearLayoutError.setVisibility(View.VISIBLE);
			}

		} catch (Exception e) {
			// any exception show the error layout
			//linearLayoutEmp.setVisibility(View.GONE);
			//linearLayoutError.setVisibility(View.VISIBLE);
			e.printStackTrace();
		}

	}
}
